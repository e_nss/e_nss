#
# NSS Engine
#
# This is free software; see Copyright file in the source
# distribution for precise wording.
#
# Copyright (C) 2019 Roumen Petrov
#

# AC_C_FUNCNAME
# -------------
# Check how CC defines current function.
AN_IDENTIFIER([__func__], [AC_C_FUNCNAME])
AC_DEFUN([AC_C_FUNCNAME],[
AC_MSG_CHECKING([whether compiler defines name of current function])
c_funcname=false
for FNAME in __func__ __FUNCTION__ ; do
  AC_LINK_IFELSE([
    AC_LANG_PROGRAM(
      [[#include <stdio.h>]],
      [[ printf("%s", $FNAME); ]]
    )],
    [ c_funcname=: ]
  )
  $c_funcname && break
done
if $c_funcname ; then
  AC_MSG_RESULT([$FNAME])
else
  AC_MSG_RESULT([none])
  FNAME='""'
fi
AH_VERBATIM([__func__],[
/* Define to `__FUNCTION__' if that is what the C compiler support, or
   empty string if "current function" is not supported under any macro. */
#undef __func__
])
if test "$FNAME" != "__func__" ; then
  cat >>confdefs.h <<_ACEOF
#define __func__ $FNAME
_ACEOF
fi
])# AC_C_FUNCNAME
