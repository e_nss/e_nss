#
# spec file for e_nss package
#
# This is free software; see Copyright file in the source
# distribution for precise wording.
#
# Copyright (c) 2019-2022 Roumen Petrov
#

# norootforbuild

Url:		https://roumenpetrov.info/e_nss/

Name:		e_nss
Summary:	OpenSSL NSS engine
Version:	4.3.1
Release:	1
License:	MIT
Group:		Productivity/Security

Requires:	openssl nspr nss

BuildRequires:	openssl-devel nspr-devel nss-devel
BuildRequires:	openssl nss-tools
BuildRoot:	%{_tmppath}/%{name}-%{version}-build

%if 0%{?rhel_version} > 0 && 0%{?rhel_version} < 600
Source0:	https://roumenpetrov.info/e_nss/e_nss-%{version}.tar.gz
%else
Source0:	https://roumenpetrov.info/e_nss/e_nss-%{version}.tar.xz
%endif

# Default values for additional components

%define openssl_enginesdir	%{_libdir}/openssl/engines

%if 0%{?rhel_version} > 0
 %define e_nss_docdir %{_docdir}/%{name}-%{version}
%else
 %define e_nss_docdir %{_docdir}/%{name}
%endif

# autoconf before 2.60 does not support --docdir
%define use_docdir 1
%if 0%{?rhel_version} > 0 && 0%{?rhel_version} < 600
 %undefine use_docdir
 %define use_docdir 0
%endif


%description
 An OpenSSL cryptographic module(engine) that use key and/or
certificates stored in Mozilla NSS(Network Security Services)
database to perform cryptographic operations.
 Module support RSA, DSA and ECDSA methods.


%changelog
# Not managed, please see source repository for changes.


%prep


%setup -q


%build
CPPFLAGS="$CPPFLAGS -I/usr/include/nspr4 -I/usr/include/nss3"
export CPPFLAGS
%configure \
%if %{use_docdir}
  --docdir=%{e_nss_docdir} \
%endif
  --with-enginesdir=%{openssl_enginesdir} \
  --enable-tests
make


%check
make check


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/
rm $RPM_BUILD_ROOT/%{openssl_enginesdir}/*e_nss.la
%if !%{use_docdir}
 %if 0%{?rhel_version} > 0
  mv $RPM_BUILD_ROOT/%{_docdir}/%{name} $RPM_BUILD_ROOT/%{_docdir}/%{name}-%{version}
 %endif
%endif


%clean
rm -rf $RPM_BUILD_ROOT


%pre


%post


%files
%defattr(-,root,root)
%doc AUTHORS Copyright INSTALL README.adoc
%doc TODO
%dir %{openssl_enginesdir}
%attr(0755,root,root) %{openssl_enginesdir}/*e_nss.so
