#
# spec file for e_nss package
#
# This is free software; see Copyright file in the source
# distribution for precise wording.
#
# Copyright (c) 2011-2020 Roumen Petrov
#

# norootforbuild

Url:		https://roumenpetrov.info/e_nss/

Name:		e_nss
Summary:	OpenSSL NSS engine
Version:	4.3.1
Release:	1
License:	MIT
Group:		Productivity/Security

Requires:	libopenssl mozilla-nspr mozilla-nss

BuildRequires:	libopenssl-devel mozilla-nspr-devel mozilla-nss-devel
BuildRequires:	openssl mozilla-nss-tools
BuildRoot:	%{_tmppath}/%{name}-%{version}-build

%if 0%{?sle_version} >= 120000
Source0:	https://roumenpetrov.info/e_nss/e_nss-%{version}.tar.xz
%else
Source0:	https://roumenpetrov.info/e_nss/e_nss-%{version}.tar.gz
%endif


# Default values for additional components

#-# The standard openssl engines are in /libXX/engines
#-# We cannot install in standard location as nss engine depend
#-# from libraries instaled in in /usr/libXX
#-#%define openssl_enginesdir	/%{_lib}/engines
%define openssl_enginesdir	%{_libdir}/engines


%description
 An OpenSSL cryptographic module(engine) that use key and/or
certificates stored in Mozilla NSS(Network Security Services)
database to perform cryptographic operations.
 Module support RSA, DSA and ECDSA methods.


%changelog
# Not managed, please see source repository for changes.


%prep


%setup -q


%build
# Creation of DSA certificate fail on SLE 11-SP4(openssl-0.9.8j-0.70.1)
# and pass of SLE 11-SP3(openssl-0.9.8j-0.50.1). As does not exist build
# macro to distinguish them let exclude DSA key method.
CPPFLAGS="$CPPFLAGS -I/usr/include/nspr4 -I/usr/include/nss3"
export CPPFLAGS
%configure \
  --docdir=%{_docdir}/%{name} \
  --with-enginesdir=%{openssl_enginesdir} \
%if 0%{?sles_version} == 11
  --disable-dsa-tests \
%endif
  --enable-tests
make


%check
make check


%install
make install DESTDIR=%{buildroot}
rm %{buildroot}/%{openssl_enginesdir}/*e_nss.la


#obsolete#%clean


%pre


%post


%files
%defattr(-,root,root)
%if 0%{?suse_version} >= 1500
%license Copyright
%endif
%doc AUTHORS Copyright INSTALL README.adoc
%doc TODO
%dir %{openssl_enginesdir}
%attr(0755,root,root) %{openssl_enginesdir}/*e_nss.so
