#! /bin/sh

set -e

scriptdir=${0%/*}

srcdir=${srcdir-$scriptdir}
p12pass=${p12pass-file:$srcdir/../p12passfile}
keypass=${keypass-file:$srcdir/../keypassfile}
OPENSSL=${OPENSSL-openssl}
genpkey=${genpkey-:}

TMPDIR=${TMPDIR-/tmp}

# ===

CA_CONFIG=$srcdir/catest.config
CERT_DAYS=60

for D in keys crt db newcerts; do
  mkdir $D 2> /dev/null || :
done
for F in \
  db/index-level1.txt \
  db/index-client.txt \
; do
  test -f $F || > $F
done

F=db/serial
test -f $F || echo '201708150907000001' > $F

if $genpkey ; then
  cipher=aes-256-cbc
  digest=sha256
else
  cipher=des3
  digest=sha1
fi


# ===

gen_key_rsa() {
  echo "-- generating RSA key $N ..." >&2
  rm -f $N
  if $genpkey ; then
    $OPENSSL genpkey -algorithm RSA -$cipher -pass $keypass -out $1-t \
     -pkeyopt rsa_keygen_bits:2048
  else
    $OPENSSL genrsa -$cipher -passout $keypass -out $N-t 2048
  fi
}

gen_key_dsa() {
  echo "-- generating DSA key parameteres $1 ..." >&2
  rm -f $1.prm
  if $genpkey ; then
    $OPENSSL genpkey -genparam -algorithm DSA -out $1.prm \
      -pkeyopt dsa_paramgen_bits:1024
  else
    $OPENSSL dsaparam -out $1.prm 1024
  fi

  echo "-- generating DSA key $1 ..." >&2
  rm -f $1
  if $genpkey ; then
    $OPENSSL genpkey -$cipher -paramfile $1.prm -pass $keypass -out $1-t
  else
    $OPENSSL gendsa -$cipher -passout $keypass -out $1-t $1.prm
  fi

  rm -f $1.prm
}

gen_key_ec() {
  echo "-- generating EC key parameteres $1 ..." >&2
  rm -f $1.prm
  if $genpkey ; then
    $OPENSSL genpkey -genparam -algorithm EC -out $1.prm \
      -pkeyopt ec_paramgen_curve:$curve
  else
    $OPENSSL ecparam -name $curve -genkey -out $1.prm
  fi

  echo "-- generating EC key $1 ..." >&2
  rm -f $1
  if $genpkey ; then
    $OPENSSL genpkey -$cipher -paramfile $1.prm -pass $keypass -out $1-t
  else
    $OPENSSL ec -$cipher -in $N.prm -passout $keypass -out $1-t
  fi

  rm -f $1.prm
}

gen_key () {
( umask 077
  T=$1
  N=$2

  case "$T" in
  rsa)
    gen_key_rsa $N;;
  dsa)
    gen_key_dsa $N;;
  ec1|ec2|ec3)
    case "$T" in
    ec1) curve=prime256v1;;
    ec2) curve=secp384r1;;
    ec3) curve=secp521r1;;
    esac
    gen_key_ec $N;;
  esac
  mv $N-t $N
)
}


gen_x509 (){
  $OPENSSL x509 -in $1 -text -nameopt utf8,sep_comma_plus > $2-t
  mv $2-t $2
}


echo_self_dn () {
  printf "\n\n\n\n\n\n\n%s\n\n" "$1"
}


echo_req_dn () {
  printf "\n\n\n\n\n\n\n%s\n\n%s\n" "$1" "$2"
}


cre_cert_root () {
  echo "creating root key and certificate ..." >&2
( N=root0

  gen_key rsa $TMPDIR/$N.key

  echo_self_dn "E_NSS Root Certificate" | \
  $OPENSSL req -new -x509 -utf8 -config "$CA_CONFIG" \
    -key $TMPDIR/$N.key -passin $keypass \
    -days $CERT_DAYS -$digest \
    -extensions ca_cert_root \
    -out $TMPDIR/$N.crt
  echo

  mv $TMPDIR/$N.key ./keys/
  gen_x509 $TMPDIR/$N.crt ./crt/$N.crt.pem
  rm -f $TMPDIR/$N.crt
) > cre_cert_root.log 2>&1
}


cre_cert_level1 () {
  echo "creating level1 key and certificate ..." >&2
( N=level1

  gen_key rsa $TMPDIR/$N.key

  echo_req_dn "E_NSS Level 1 Certificate" "" | \
  $OPENSSL req -new -utf8 -config "$CA_CONFIG" \
    -key $TMPDIR/$N.key -passin $keypass \
    -out $TMPDIR/$N.csr
  echo

  $OPENSSL ca -batch -config "$CA_CONFIG" \
    -in $TMPDIR/$N.csr \
    -name ca_level1 -passin $keypass \
    -days $CERT_DAYS -md $digest \
    -out $TMPDIR/$N.crt

  mv $TMPDIR/$N.key ./keys/
  gen_x509  $TMPDIR/$N.crt ./crt/$N.crt.pem
  rm -f $TMPDIR/$N.crt
  rm -f $TMPDIR/$N.csr
) > cre_cert_level1.log 2>&1
}


get_name () {
  case "$1" in
  rsa|dsa|ec1|ec2|ec3);;
  *) return 1;
  esac
  echo client-$1
}


cre_client_key () {
  T=$1
  echo "creating client key ($T) ..." >&2
  N=`get_name $T`
( shift
  gen_key $T $TMPDIR/$N.key

  mv $TMPDIR/$N.key ../$N-priv.pem
) > cre_client_key-$T.log 2>&1
}


cre_client_crt () {
  T=$1
  echo "creating client certificate ($T) ..." >&2
  N=`get_name $T`
( shift

  echo_req_dn "E_NSS Certificate ($N)" "$GET_PASS" | \
  $OPENSSL req -new -utf8 -config "$CA_CONFIG" \
    -key ../$N-priv.pem -passin $keypass \
    -out $TMPDIR/$N.csr
  echo

  $OPENSSL ca -batch -config "$CA_CONFIG" \
    -in $TMPDIR/$N.csr \
    -name ca_client -passin $keypass \
    -days $CERT_DAYS -md $digest \
    -out $TMPDIR/$N.crt

  gen_x509  $TMPDIR/$N.crt ../$N-cert.pem
  rm -f $TMPDIR/$N.crt
  rm -f $TMPDIR/$N.csr
) > cre_client_crt-$T.log 2>&1
}


cre_client_p12 () {
  T=$1
  echo "creating client pkcs#12 ($T) ..." >&2
  N=`get_name $T`
  shift

  ( cat ../$N-priv.pem
    cat ../$N-cert.pem
    cat ./crt/level1.crt.pem
    cat ./crt/root0.crt.pem
  ) | \
  $OPENSSL pkcs12 -export \
    -passin $keypass -passout $p12pass \
    -out $TMPDIR/$N.p12
  mv  $TMPDIR/$N.p12 ../
}


cre_client_pub () {
  T=$1
  echo "extracting client public key ($T) ..." >&2
  N=`get_name $T`
( shift

  if $genpkey ; then
    cmd=pkey
  else
    case $T in
    rsa) cmd=rsa;;
    dsa) cmd=dsa;;
    ec*) cmd=ec;;
    esac
  fi
  $OPENSSL $cmd -pubout \
    -in ../$N-priv.pem -passin $keypass \
    -out $TMPDIR/$N.pub
  mv $TMPDIR/$N.pub ../$N-pub.pem
) > /dev/null 2>&1
}


# ===

case "$1" in
root) cre_cert_root;;
lvl1) cre_cert_level1;;
key|crt|p12|pub) (
  N=$1
  shift
  cre_client_$N ${1+"$@"}
  ) ;;
*) exit 1;;
esac
