#ifndef HEADER_NSS_ERR_H
#define HEADER_NSS_ERR_H
/**
 * NSS Engine - errors
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2011-2021 Roumen Petrov
 */

void
ERR_NSS_error(const char *file, int line, const char *funcname, int function, int reason);

#define NSSerr(f,r)   ERR_NSS_error(__FILE__, __LINE__, __func__, (f), (r))

/* Functions */
#define NSS_F_INIT					 100
#define NSS_F_FINISH					 101
#define NSS_F_CMD_CONFIG_DIR				 102
#define NSS_F_CMD_LIST_CERT				 103
#define NSS_F_CMD_EVP_CERT				 104
#define NSS_F_INIT_KEYCTX				 110
#define NSS_F_LOAD_KEY					 111
#define NSS_F_GET_CERT					 112
#define NSS_F_STORE_OPEN				 113

#define NSS_F_RSA_INIT					 130
#define NSS_F_RSA_PRIV_DEC				 131
#define NSS_F_RSA_PRIV_ENC				 132
#define NSS_F_RSA_SIGN					 133
#define NSS_F_RSA_VERIFY				 134
#define NSS_F_DSA_INIT					 135
#define NSS_F_DSA_DO_SIGN				 136
#define NSS_F_ECDSA_DO_SIGN				 137
#define NSS_F_ECDSA_DO_VERIFY				 138
#define NSS_F_EC_INIT					 139

/* Reasons */
#define NSS_R_ENG_CTX_INDEX				 100
#define NSS_R_DB_IS_NOT_INITIALIZED			 101
#define NSS_R_CANNOT_SETUP_CONFIG_DIR			 102
#define NSS_R_CONFIG_DIR_IS_SET				 103
#define NSS_R_NOT_IN_FIPS_MODE				 104
#define NSS_R_SHUTDOWN_FAIL				 109

#define NSS_R_INSUFFICIENT_MEMORY			 110
#define NSS_R_INVALID_ARGUMENT				 112
#define NSS_R_INVALID_ALGORITHM				 113
#define NSS_R_NOT_SUPPORTED				 114
#define NSS_R_UNSUPPORTED_KEYTYPE			 115
#define NSS_R_UNSUPPORTED_NID				 116
#define NSS_R_UNSUPPORTED_PADDING			 117
#define NSS_R_DISABLED_ALGORITHM			 118

#define NSS_R_MISSING_KEY_CONTEXT			 120
#define NSS_R_MISSING_CERT				 121
#define NSS_R_MISSING_PUBKEY				 122
#define NSS_R_MISSING_PVTKEY				 123
#define NSS_R_DERENCODE_PUBKEY				 124
#define NSS_R_DERENCODE_PUBKEYBUF			 125

#define NSS_R_DECRYPT_FAIL				 130
#define NSS_R_SGN_DIGEST_FAIL				 131
#define NSS_R_VERIFY_DIGEST_FAIL			 132
#define NSS_R_BAD_SIGNATURE				 133
#define NSS_R_INVALID_DIGEST_LENGTH			 134
#define NSS_R_INVALID_SIGNATURE_LENGTH			 135

#endif /*ndef HEADER_NSS_ERR_H*/
