/**
 * NSS Engine - RSA method
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2011-2023 Roumen Petrov
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#ifndef USE_OPENSSL_PROVIDER
/* TODO: implement OpenSSL 4.0 API, as OpenSSL 3.* is quite nonfunctional */
# define OPENSSL_SUPPRESS_DEPRECATED
#endif

#ifdef E_NSS_RSA_METHOD
/* x509 is used below and is preferred to rsa, evp and etc. / 0.9.7 */
#define OPENSSL_OCSP_H /* see e_nss_store.c */
#include <openssl/x509.h>
#include <openssl/engine.h>

#include "e_nss_int.h"
#include "e_nss_err.h"

#include <nss.h>
#include <cryptohi.h>
#include <secerr.h>
/* does not exist in "old" versions */
#define SEC_ERROR_SIGNATURE_ALGORITHM_DISABLED	(SEC_ERROR_BASE + 181)


/* ===== OpenSSL RSA method compatibility code ===== */

#if defined(OPENSSL_VERSION_NUMBER) && (OPENSSL_VERSION_NUMBER < 0x10000000L)
/* NOTE RSA verify with non-const sigbuf argument in OpenSSL < 1.0 */
# define CONST_RSA_SIGBUF
#else
# define CONST_RSA_SIGBUF const
#endif

#ifndef HAVE_RSA_PKCS1_OPENSSL	/* OpenSSL < 1.1 */
static inline const RSA_METHOD*
RSA_PKCS1_OpenSSL(void) {
    return RSA_PKCS1_SSLeay();
}
#endif /*ndef HAVE_RSA_PKCS1_OPENSSL*/

#ifndef HAVE_RSA_METH_NEW
/* OpenSSL >= 1.1 by commit "Make the RSA_METHOD structure opaque",
 * and etc.
 */

/* opaque RSA method */
static inline RSA_METHOD*
RSA_meth_new(const char *name, int flags) {
    RSA_METHOD *meth;

    meth = OPENSSL_malloc(sizeof(RSA_METHOD));
    if (meth == NULL) return NULL;

    memset(meth, 0, sizeof(*meth));
    meth->name = NSS_strdup(name);
    meth->flags = flags;

    return(meth);
}


static inline void
RSA_meth_free(RSA_METHOD *meth) {
    if (meth == NULL) return;

    if (meth->name != NULL)
        OPENSSL_free((char*)meth->name);
    OPENSSL_free(meth);
}


static inline const char*
RSA_meth_get0_name(const RSA_METHOD *meth) { return meth->name; }


typedef int (*RSA_METHOD_priv_enc_f) (int flen, const unsigned char *from,
    unsigned char *to, RSA *rsa, int padding);

static inline int
RSA_meth_set_priv_enc(RSA_METHOD *meth, RSA_METHOD_priv_enc_f priv_enc) {
    meth->rsa_priv_enc = priv_enc;
    return 1;
}


typedef int (*RSA_METHOD_priv_dec_f) (int flen, const unsigned char *from,
    unsigned char *to, RSA *rsa, int padding);

static inline int
RSA_meth_set_priv_dec(RSA_METHOD *meth, RSA_METHOD_priv_dec_f priv_dec) {
    meth->rsa_priv_dec = priv_dec;
    return 1;
}


typedef int (*RSA_METHOD_init_f) (RSA *rsa);

static inline int
RSA_meth_set_init(RSA_METHOD *meth, RSA_METHOD_init_f init) {
    meth->init = init;
    return 1;
}


typedef int (*RSA_METHOD_sign_f) (int type,
    const unsigned char *m, unsigned int m_length,
    unsigned char *sigret, unsigned int *siglen, const RSA *rsa);

static inline int
RSA_meth_set_sign(RSA_METHOD *meth, RSA_METHOD_sign_f sign) {
    meth->rsa_sign = sign;
    return 1;
}


typedef int (*RSA_METHOD_verify_f) (int dtype,
    const unsigned char *m, unsigned int m_length,
    CONST_RSA_SIGBUF
    unsigned char *sigbuf, unsigned int siglen, const RSA *rsa);

static inline int
RSA_meth_set_verify(RSA_METHOD *meth, RSA_METHOD_verify_f verify) {
    meth->rsa_verify = verify;
    return 1;
}


typedef int (*RSA_METHOD_pub_enc_f) (int flen, const unsigned char *from,
    unsigned char *to, RSA *rsa, int padding);

static inline RSA_METHOD_pub_enc_f
RSA_meth_get_pub_enc(const RSA_METHOD *meth) { return meth->rsa_pub_enc; }

static inline int
RSA_meth_set_pub_enc(RSA_METHOD *meth, RSA_METHOD_pub_enc_f pub_enc) {
    meth->rsa_pub_enc = pub_enc;
    return 1;
}


typedef int (*RSA_METHOD_pub_dec_f) (int flen, const unsigned char *from,
    unsigned char *to, RSA *rsa, int padding);

static inline RSA_METHOD_pub_dec_f
RSA_meth_get_pub_dec(const RSA_METHOD *meth) { return meth->rsa_pub_dec; }

static inline int
RSA_meth_set_pub_dec(RSA_METHOD *meth, RSA_METHOD_pub_dec_f pub_dec) {
    meth->rsa_pub_dec = pub_dec;
    return 1;
}


typedef int (*RSA_METHOD_rsa_mod_exp_f) (
    BIGNUM *r0, const BIGNUM *I, RSA *rsa
#if defined(OPENSSL_VERSION_NUMBER) && (OPENSSL_VERSION_NUMBER < 0x00908000L)
/* NOTE missing BN_CTX argument in OpenSSL < 0.9.8 */
#else
    , BN_CTX *ctx
#endif
);

static inline RSA_METHOD_rsa_mod_exp_f
RSA_meth_get_mod_exp(const RSA_METHOD *meth) { return meth->rsa_mod_exp; }

static inline int
RSA_meth_set_mod_exp(RSA_METHOD *meth, RSA_METHOD_rsa_mod_exp_f rsa_mod_exp ) {
    meth->rsa_mod_exp = rsa_mod_exp;
    return 1;
}


typedef int (*RSA_METHOD_rsa_bn_mod_exp_f) (BIGNUM *r, const BIGNUM *a,
    const BIGNUM *p, const BIGNUM *m, BN_CTX *ctx,
    BN_MONT_CTX *m_ctx);

static inline RSA_METHOD_rsa_bn_mod_exp_f
RSA_meth_get_bn_mod_exp(const RSA_METHOD *meth) { return meth->bn_mod_exp; }

static inline int
RSA_meth_set_bn_mod_exp(RSA_METHOD *meth, RSA_METHOD_rsa_bn_mod_exp_f bn_mod_exp) {
    meth->bn_mod_exp = bn_mod_exp;
    return 1;
}


/* opaque RSA key */
static inline const ENGINE*
RSA_get0_engine(const RSA *rsa) { return rsa->engine; }


static inline void
RSA_get0_key(const RSA *rsa, const BIGNUM **n, const BIGNUM **e, const BIGNUM **d) {
    if (n != NULL) *n = rsa->n;
    if (e != NULL) *e = rsa->e;
    if (d != NULL) *d = rsa->d;
}


static inline int
RSA_set0_key(RSA *rsa, BIGNUM *n, BIGNUM *e, BIGNUM *d) {
    /* If the fields in r are NULL, the corresponding input
     * parameters MUST be non-NULL for n and e.  d may be
     * left NULL (in case only the public key is used).
     *
     * It is an error to give the results from get0 on r
     * as input parameters.
     */
    if (n == rsa->n || e == rsa->e
        || (rsa->d != NULL && d == rsa->d))
        return 0;

    if (n != NULL) { BN_free(rsa->n); rsa->n = n; }
    if (e != NULL) { BN_free(rsa->e); rsa->e = e; }
    if (d != NULL) { BN_free(rsa->d); rsa->d = d; }

    return 1;
}
#endif /*ndef HAVE_RSA_METH_NEW*/


#ifndef HAVE_X509_SIG_GET0
/* OpenSSL >= 1.1 by commit "Make X509_SIG opaque".
 * changed by commit "Constify X509_SIG".
 */
static inline void
X509_SIG_get0(const X509_SIG *sig, const X509_ALGOR **palg,
    const ASN1_OCTET_STRING **pdigest
) {
    if (palg)
        *palg = sig->algor;
    if (pdigest)
        *pdigest = sig->digest;
}
#endif /*ndef HAVE_X509_SIG_GET0*/


/* ===== NSS RSA method ===== */

static int nss_rsa_ctx_index = -1;


static inline NSS_KEYCTX*
nss_get_keyctx(const RSA *key) {
    return RSA_get_ex_data(key, nss_rsa_ctx_index);
}

NSS_KEYCTX*
nss_get_keyctx_rsa(EVP_PKEY *pkey) {
    NSS_KEYCTX *ret;
{/* OpenSSL >= 1.1 by commit "Add EVP_PKEY_get0_* functions."
    adds EVP_PKEY_get0_RSA with not constant return value.
    In 3.0 definition is changed to return constant!
    So throw away get0 and use existing since 0.9.5 get1 function
    with non significant performance penalty.
  */
    RSA *rsa = EVP_PKEY_get1_RSA(pkey);
    ret = nss_get_keyctx(rsa);
    RSA_free(rsa);
}
    return ret;
}


static void
NSS_KEYCTX_free_rsa(
    void *parent, void *ptr, CRYPTO_EX_DATA *ad,
    int idx, long argl, void *argp
) {
    UNUSED(parent); UNUSED(ad); UNUSED(argl); UNUSED(argp);

    CALL_TRACE("idx: %d, ptr: %p\n", idx, ptr);
    if (idx == nss_rsa_ctx_index)
         NSS_KEYCTX_free(ptr);
}


static inline NSS_CTX*
RSA_get_NSS_CTX(const RSA *key) {
    const ENGINE *e = RSA_get0_engine(/*save cast*/(RSA*)key);
    return ENGINE_get_NSS_CTX(e);
}


static int/*bool*/
nss_rsa_sign(int dtype, const unsigned char *m, unsigned int m_length, unsigned char *sigret, unsigned int *siglen, const RSA *rsa);


static int
nss_rsa_priv_enc_pkcs1(int len, const unsigned char *from, unsigned char *to, RSA *rsa) {
    int                  ret = -1;
    X509_SIG            *sig;
    const unsigned char *m;
    unsigned int         m_length;

{
#if defined(OPENSSL_VERSION_NUMBER) && (OPENSSL_VERSION_NUMBER < 0x00908000L)
    unsigned char *q = (unsigned char *)from;
#else
    const unsigned char *q = from;
#endif
#ifdef HAVE_ERR_SET_MARK
    /* OpenSSL >= 0.9.8 */
    (void)ERR_set_mark();
#endif
    sig = d2i_X509_SIG(NULL, &q, RSA_size(rsa));
#ifdef HAVE_ERR_SET_MARK
    (void)ERR_pop_to_mark();
#else
    /* Lets clear all errors for outdated OpenSSL versions (<0.9.8) */
    ERR_clear_error();
#endif
}
    /* use "sign" method only if input is X.509 signature! */
    if (sig == NULL) {
        NSSerr(NSS_F_RSA_PRIV_ENC, NSS_R_NOT_SUPPORTED);
        return -1;
    }
    if (len != i2d_X509_SIG(sig, NULL)) goto done;

{   const ASN1_OCTET_STRING *digest;

    X509_SIG_get0(sig, NULL, &digest);
    if (digest == NULL) goto done;

    m = digest->data;
    if (m == NULL) goto done;

    m_length = digest->length;
}

{
    unsigned char *sigret = to;
    unsigned int   siglen;

    if (nss_rsa_sign(NID_sha1, m, m_length, sigret, &siglen, rsa) != 0)
        ret = siglen;
}

done:
    if (sig != NULL)
        X509_SIG_free(sig);
    return ret;
}


static int
nss_rsa_priv_enc(int len, const unsigned char *from, unsigned char *to, RSA *rsa, int padding) {
    if (padding == RSA_PKCS1_PADDING)
       return nss_rsa_priv_enc_pkcs1(len, from, to, rsa);

    NSSerr(NSS_F_RSA_PRIV_ENC, NSS_R_NOT_SUPPORTED);
    return -1;
}


#ifdef HAVE_PK11_PRIVDECRYPT
static int
nss_rsa_priv_dec(int len, const unsigned char *from, unsigned char *to, RSA *rsa, int padding) {
    int         ret = -1;
    NSS_CTX    *ctx;
    NSS_KEYCTX *keyctx;
    CK_MECHANISM_TYPE mechanism;

    switch(padding) {
    case RSA_NO_PADDING: mechanism = CKM_RSA_X_509; break;
    case RSA_PKCS1_PADDING: mechanism = CKM_RSA_PKCS; break;
    case RSA_PKCS1_OAEP_PADDING: mechanism = CKM_RSA_PKCS_OAEP; break;
    default: {
        NSSerr(NSS_F_RSA_PRIV_DEC, NSS_R_UNSUPPORTED_PADDING);
        {/*add extra error message data*/
            char msgstr[10];
            BIO_snprintf(msgstr, sizeof(msgstr), "%d", padding);
            ERR_add_error_data(2, "PADDING=", msgstr);
        }
        goto done;
        } break;
    }
    ctx = RSA_get_NSS_CTX(rsa);
    keyctx = nss_get_keyctx(rsa);

    nss_trace(ctx, "keyctx=%p\n", (void*)keyctx);
    if (keyctx == NULL) {
        NSSerr(NSS_F_RSA_PRIV_DEC, NSS_R_MISSING_KEY_CONTEXT);
        goto done;
    }
    nss_trace(ctx, "keyctx->pvtkey=%p\n", (void*)keyctx->pvtkey);
    if (keyctx->pvtkey == NULL) {
        NSSerr(NSS_F_RSA_PRIV_DEC, NSS_R_MISSING_PVTKEY);
        goto done;
    }

    {
        static CK_RSA_PKCS_OAEP_PARAMS oaep_params =
            { CKM_SHA_1, CKG_MGF1_SHA1, CKZ_DATA_SPECIFIED, NULL, 0};

        SECStatus      rv = SECFailure;
        SECItem        param = { siBuffer, NULL , 0};
        unsigned char *enc = (unsigned char*)from;
        unsigned int   outLen;
        unsigned int   maxLen = RSA_size(rsa);

        switch(mechanism) {
        case CKM_RSA_PKCS_OAEP: {
            param.data = (unsigned char*) &oaep_params;
            param.len  = sizeof(oaep_params);
            } break;
        default : break;
        }

        rv = PK11_PrivDecrypt(keyctx->pvtkey, mechanism, &param, to, &outLen, maxLen, enc, len);
        if (rv != SECSuccess) {
            NSSerr(NSS_F_RSA_PRIV_DEC, NSS_R_DECRYPT_FAIL);
            goto done;
        }
        ret = outLen;
    }
done:
    return ret;
}
#endif /*def HAVE_PK11_PRIVDECRYPT*/


static int
nss_rsa_init(RSA *key) {
    int ret = 0;

    CALL_TRACE("key=%p\n", (void*)key);

{   /* setup NSS RSA key context */
    NSS_KEYCTX *keyctx = NSS_KEYCTX_new();
    CALL_TRACE("idx=%d, keyctx=%p\n", nss_rsa_ctx_index, (void*)keyctx);
    if (keyctx == NULL) {
        NSSerr(NSS_F_RSA_INIT, NSS_R_INSUFFICIENT_MEMORY);
        goto done;
    }
    RSA_set_ex_data(key, nss_rsa_ctx_index, keyctx);
}

    ret = 1;
done:
    CALL_TRACE("^ %s\n", (ret ? "ok": ""));
    return ret;
}


static int/*bool*/
nss_rsa_sign(int dtype, const unsigned char *m, unsigned int m_length, unsigned char *sigret, unsigned int *siglen, const RSA *rsa) {
    int         ret = 0;
    SECStatus   rv;
    NSS_CTX    *ctx;
    NSS_KEYCTX *keyctx;

    ctx = RSA_get_NSS_CTX(rsa);
    nss_trace(ctx, "args: dtype=%d, m=%p, m_length=%d\n", dtype, (void*)m, m_length);
{
    const RSA_METHOD *rsa_meth = RSA_get_method(rsa);
    nss_trace(ctx, "rsa=%p, meth=%p, name=%s\n", (void*)rsa, (void*)rsa_meth,
        RSA_meth_get0_name(rsa_meth));
}

    keyctx = nss_get_keyctx(rsa);
    nss_trace(ctx, "keyctx=%p\n", (void*)keyctx);
    if (keyctx == NULL) {
        NSSerr(NSS_F_RSA_SIGN, NSS_R_MISSING_KEY_CONTEXT);
        goto done;
    }

    nss_trace(ctx, "keyctx->pvtkey=%p\n", (void*)keyctx->pvtkey);
    if (keyctx->pvtkey == NULL) {
        NSSerr(NSS_F_RSA_SIGN, NSS_R_MISSING_PVTKEY);
        goto done;
    }

    nss_trace(ctx, "keyctx->pvtkey->keyType=%d\n", keyctx->pvtkey->keyType);

{   /* compute nss signature */
    SECItem    digest;
    SECItem    result;
    SECOidTag  hashalg;

    digest.type = siBuffer;
    digest.data = (unsigned char*)m;
    digest.len  = m_length;

    result.type = siBuffer;
    result.data = NULL;
    result.len  = 0;

    switch (dtype) {
    case NID_md2        : hashalg = SEC_OID_MD2   ; break;
    case NID_md5        : hashalg = SEC_OID_MD5   ; break;
#ifndef SHA256_DIGEST_LENGTH	/* OpenSSL < 0.9.8 */
    case NID_sha1       : hashalg = SEC_OID_SHA1  ; break;
#else
    case NID_sha1       : {
        /* Work-around for broken digest type in OpenSSL 1.0* FIPS mode. */
        switch (m_length) {
        case SHA_DIGEST_LENGTH   : hashalg = SEC_OID_SHA1  ; break;
        case SHA256_DIGEST_LENGTH: hashalg = SEC_OID_SHA256; break;
        case SHA384_DIGEST_LENGTH: hashalg = SEC_OID_SHA384; break;
        case SHA512_DIGEST_LENGTH: hashalg = SEC_OID_SHA512; break;	
        default: {
            NSSerr(NSS_F_RSA_SIGN, NSS_R_UNSUPPORTED_NID);
            {/*add extra error message data*/
                char msgstr[30];
                BIO_snprintf(msgstr, sizeof(msgstr), "%d, length=%u", dtype, m_length);
                ERR_add_error_data(2, "NID=", msgstr);
            }
            goto done_sign;
            }
	}
	} break;
#endif
    case NID_sha256     : hashalg = SEC_OID_SHA256; break;
    case NID_sha384     : hashalg = SEC_OID_SHA384; break;
    case NID_sha512     : hashalg = SEC_OID_SHA512; break;
    default : {
        NSSerr(NSS_F_RSA_SIGN, NSS_R_UNSUPPORTED_NID);
        {/*add extra error message data*/
            char msgstr[10];
            BIO_snprintf(msgstr, sizeof(msgstr), "%d", dtype);
            ERR_add_error_data(2, "NID=", msgstr);
        }
        goto done_sign;
        } break;
    }

    nss_trace(ctx, "sigret=%p\n", (void*)sigret);

/* NOTE:
 * - If we use PK11_Sign here we must use PK11_Verify later
 *   but the signed data is not compatible with openssl
 * - SEC_SignData may be hash again hash data
 *   http://www.mail-archive.com/dev-tech-crypto@lists.mozilla.org/msg09116.html
 */
#if 0
/*
** Sign a single block of data using private key encryption and given
** signature/hash algorithm.
**	"result" the final signature data (memory is allocated)
**	"buf" the input data to sign
**	"len" the amount of data to sign
**	"pk" the private key to encrypt with
**	"algid" the signature/hash algorithm to sign with
**		(must be compatible with the key type).
*/
extern SECStatus SEC_SignData(SECItem *result,
			     const unsigned char *buf, int len,
			     SECKEYPrivateKey *pk, SECOidTag algid);
#endif

    rv = SGN_Digest(keyctx->pvtkey, hashalg, &result, &digest);
    nss_trace(ctx, "rv=%d\n", rv);
    if (rv != SECSuccess) {
        int port_err = PORT_GetError();
        switch(port_err) {
        case SEC_ERROR_INVALID_ALGORITHM: {
            NSSerr(NSS_F_RSA_SIGN, NSS_R_INVALID_ALGORITHM);
            } break;
        case SEC_ERROR_SIGNATURE_ALGORITHM_DISABLED: {
            NSSerr(NSS_F_RSA_SIGN, NSS_R_DISABLED_ALGORITHM);
            } break;
        default: {
            int port_err_off = port_err - SEC_ERROR_BASE;

            nss_trace(ctx, "port_err/ofset=%d/%d\n", port_err, port_err_off);
            NSSerr(NSS_F_RSA_SIGN, NSS_R_SGN_DIGEST_FAIL);
            {/*add extra error message data*/
                char msgstr[10];
                BIO_snprintf(msgstr, sizeof(msgstr), "%d", port_err_off);
                ERR_add_error_data(2, "PORT_ERROR_OFFSET=", msgstr);
            }
            } break;
        }
        goto done_sign;
    }

    { /* propagate result */
        int len = result.len;

        nss_trace(ctx, "sigret=%p len=%d\n", (void*)sigret, len);
        memcpy(sigret, result.data, len);
        *siglen = len;
    }

    ret = 1;

done_sign:
    if (result.data != NULL) {
        PORT_Free(result.data);
    }
}   /* end of nss signature computation */

done:
    nss_trace(ctx, "ret=%d\n", ret);
    return ret;
}


static int/*bool*/
nss_rsa_verify(
    int dtype,
    const unsigned char *m, unsigned int m_length,
    CONST_RSA_SIGBUF
    unsigned char *sigbuf, unsigned int siglen, const RSA *rsa
) {
    int         ret = 0;
    SECStatus   rv;
    NSS_CTX    *ctx;
    NSS_KEYCTX *keyctx;

    ctx = RSA_get_NSS_CTX(rsa);
    nss_trace(ctx, "args: dtype=%d\n", dtype);
{
    const RSA_METHOD *rsa_meth = RSA_get_method(rsa);
    nss_trace(ctx, "rsa=%p, meth=%p, name=%s\n", (void*)rsa, (void*)rsa_meth,
        RSA_meth_get0_name(rsa_meth));
}

    keyctx = nss_get_keyctx(rsa);
    nss_trace(ctx, "keyctx=%p\n", (void*)keyctx);
    if (keyctx == NULL) {
        NSSerr(NSS_F_RSA_VERIFY, NSS_R_MISSING_KEY_CONTEXT);
        goto done;
    }

    nss_trace(ctx, "keyctx->pubkey=%p\n", (void*)keyctx->pubkey);
    if (keyctx->pubkey == NULL) {
        NSSerr(NSS_F_RSA_VERIFY, NSS_R_MISSING_PUBKEY);
        goto done;
    }

    nss_trace(ctx, "keyctx->pubkey->keyType=%d\n", keyctx->pubkey->keyType);

{   /* nss signature verification */
    SECItem    digest;
    SECItem    sig;
    SECOidTag  algid;

    digest.type = siBuffer;
    digest.data = (unsigned char*)m;
    digest.len  = m_length;

    sig.type = siBuffer;
    sig.data = (unsigned char*)sigbuf;
    sig.len  = siglen;

    switch (dtype) {
    case NID_md2        : algid = SEC_OID_PKCS1_MD2_WITH_RSA_ENCRYPTION   ; break;
    case NID_md5        : algid = SEC_OID_PKCS1_MD5_WITH_RSA_ENCRYPTION   ; break;
    case NID_sha1       : algid = SEC_OID_PKCS1_SHA1_WITH_RSA_ENCRYPTION  ; break;
    case NID_sha256     : algid = SEC_OID_PKCS1_SHA256_WITH_RSA_ENCRYPTION; break;
    case NID_sha384     : algid = SEC_OID_PKCS1_SHA384_WITH_RSA_ENCRYPTION; break;
    case NID_sha512     : algid = SEC_OID_PKCS1_SHA512_WITH_RSA_ENCRYPTION; break;
    default : {
        NSSerr(NSS_F_RSA_VERIFY, NSS_R_UNSUPPORTED_NID);
        {/* add extra error message data*/
            char msgstr[10];
            BIO_snprintf(msgstr, sizeof(msgstr), "%d", dtype);
            ERR_add_error_data(2, "NID=", msgstr);
        }
        goto done;
        } break;
    }

#if 0
NOTE
/*
** Verify the signature on a block of data for which we already have
** the digest. The signature data is an RSA private key encrypted
** block of data formatted according to PKCS#1.
**  This function is deprecated. Use VFY_VerifyDigestDirect or
**  VFY_VerifyDigestWithAlgorithmID instead.
** 	"dig" the digest
** 	"key" the public key to check the signature with
** 	"sig" the encrypted signature data
**	"sigAlg" specifies the signing algorithm to use.  This must match
**	    the key type.
**	"wincx" void pointer to the window context
**/
extern SECStatus VFY_VerifyDigest(SECItem *dig, SECKEYPublicKey *key,
				  SECItem *sig, SECOidTag sigAlg, void *wincx);
#endif

{   NSS_UI wincx = { UI_get_default_method(), NULL };
    rv = VFY_VerifyDigest(&digest, keyctx->pubkey, &sig, algid, &wincx);
}
    nss_trace(ctx, "rv=%d\n", (int)rv);
    if (rv != SECSuccess) {
        int port_err = PORT_GetError();

        switch(port_err) {
        case SEC_ERROR_BAD_SIGNATURE: {
            NSSerr(NSS_F_RSA_VERIFY, NSS_R_BAD_SIGNATURE);
            } break;
        case SEC_ERROR_INVALID_ALGORITHM: {
            NSSerr(NSS_F_RSA_VERIFY, NSS_R_INVALID_ALGORITHM);
            } break;
        case SEC_ERROR_SIGNATURE_ALGORITHM_DISABLED: {
            NSSerr(NSS_F_RSA_VERIFY, NSS_R_DISABLED_ALGORITHM);
            } break;
        default: {
            int port_err_off = port_err - SEC_ERROR_BASE;

            nss_trace(ctx, "port_err/ofset=%d/%d\n", port_err, port_err_off);
            NSSerr(NSS_F_RSA_VERIFY, NSS_R_VERIFY_DIGEST_FAIL);
            {/*add extra error message data*/
                char msgstr[10];
                BIO_snprintf(msgstr, sizeof(msgstr), "%d", port_err_off);
                ERR_add_error_data(2, "PORT_ERROR_OFFSET=", msgstr);
            }
            } break;
        }
        goto done;
    }
}   /* end of nss signature verification */

    ret = 1;
done:
    nss_trace(ctx, "ret=%d\n", ret);
    return ret;
}


NSS_KEYCTX*
nss_init_keyctx_rsa(EVP_PKEY *pkey, ENGINE *e) {
    NSS_KEYCTX *ret = NULL;
    RSA *rsa;
    RSA *pkey_rsa;

    pkey_rsa = EVP_PKEY_get1_RSA(pkey);
    CALL_TRACE("pkey=%p, key=%p\n", (void*)pkey, (void*)pkey_rsa);
#ifdef HAVE_EVP_KEYMGMT_GET0_PROVIDER	/* OpenSSL >= 3.0 */
    /* FIPS enabled OpenSSL 3+ fail to return "elementary" key */
    if (pkey_rsa == NULL) {
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INVALID_ARGUMENT);
        return NULL;
    }
#endif

/* NOTE: OpenSSL 3+ wrongly assumes that key associated by
 * default with "external" method is a OpenSSL managed key!
 * So log only and recreate key.
 */
    if (ENGINE_get_RSA(e) == RSA_get_method(pkey_rsa)) {
        CALL_TRACE("RSA by default!\n");
#ifndef HAVE_EVP_KEYMGMT_GET0_PROVIDER	/* OpenSSL 3+ */
        ret = nss_get_keyctx(pkey_rsa);
        RSA_free(pkey_rsa);
        return ret;
#endif
    }
    /* else rsa method is not associated with this engine ... */
    CALL_TRACE("new RSA ...\n");

    rsa = RSA_new_method(e);
    if (rsa == NULL) {
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INSUFFICIENT_MEMORY);
        goto err;
    }

{ /* copy public key */
    BIGNUM *n, *e;

    RSA_get0_key(pkey_rsa, (const BIGNUM**)&n, (const BIGNUM**)&e, NULL);

    n = BN_dup(n);
    e = BN_dup(e);
    if ((n == NULL) || (e == NULL)
    ||  !RSA_set0_key(rsa, n, e, NULL)
    ) {
        BN_free(n);
        BN_free(e);
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INSUFFICIENT_MEMORY);
        goto err;
    }
}

    if (EVP_PKEY_set1_RSA(pkey, rsa))
        ret = nss_get_keyctx(rsa);

err:
    RSA_free(pkey_rsa);
    RSA_free(rsa);
    return ret;
}


int/*bool*/
bind_nss_rsa_method(ENGINE *e) {
    RSA_METHOD *nss_rsa_method = RSA_meth_new("NSS PKCS#1 RSA method",
#ifdef RSA_FLAG_SIGN_VER
/* removed in OpenSSL 1.1
 * unused in OpenSSL after 1.0.1r and 1.0.2f
 */
    RSA_FLAG_SIGN_VER |
#endif
#if 0
    /*unused if method implements sign and verify*/
    RSA_FLAG_EXT_PKEY |
#endif
#if 0
    /* - don't check pub/private match ?*/
    RSA_METHOD_FLAG_NO_CHECK |
#endif
#ifdef RSA_FLAG_FIPS_METHOD
    RSA_FLAG_FIPS_METHOD |
#endif
    0  /* int flags; */
    );
    if (nss_rsa_method == NULL) return 0;

    /* ensure RSA context index */
    if (nss_rsa_ctx_index < 0)
        nss_rsa_ctx_index = RSA_get_ex_new_index(0,
            NULL, NULL, NULL, NSS_KEYCTX_free_rsa);

    CALL_TRACE("nss_rsa_ctx_index=%d\n", nss_rsa_ctx_index);
    if (nss_rsa_ctx_index < 0) goto err;

    if (!RSA_meth_set_init(nss_rsa_method, nss_rsa_init)
    ||  !RSA_meth_set_sign(nss_rsa_method, nss_rsa_sign)
    ||  !RSA_meth_set_verify(nss_rsa_method, nss_rsa_verify)
    ||  !RSA_meth_set_priv_enc(nss_rsa_method, nss_rsa_priv_enc)
#ifdef HAVE_PK11_PRIVDECRYPT
    ||  !RSA_meth_set_priv_dec(nss_rsa_method, nss_rsa_priv_dec)
#endif
    )
        goto err;

{
    const RSA_METHOD *rsa_method = RSA_PKCS1_OpenSSL();

    CALL_TRACE("rsa_method=%p[%s]\n", (void*)rsa_method,
        (rsa_method ? RSA_meth_get0_name(rsa_method) : "?"));
    if (!RSA_meth_set_pub_enc(nss_rsa_method, RSA_meth_get_pub_enc(rsa_method))
    ||  !RSA_meth_set_pub_dec(nss_rsa_method, RSA_meth_get_pub_dec(rsa_method))
    ||  !RSA_meth_set_mod_exp(nss_rsa_method, RSA_meth_get_mod_exp(rsa_method))
    ||  !RSA_meth_set_bn_mod_exp(nss_rsa_method, RSA_meth_get_bn_mod_exp(rsa_method))
    )
        goto err;
}

    if (ENGINE_set_RSA(e, nss_rsa_method))
        return 1;

err:
    RSA_meth_free(nss_rsa_method);
    return 0;
}


void
destroy_nss_rsa_method(ENGINE *e) {
    RSA_METHOD *rsa_method;

    rsa_method = (RSA_METHOD*)ENGINE_get_RSA(e);
    (void)ENGINE_set_RSA(e, NULL);
    RSA_meth_free(rsa_method);
}
#endif /*def E_NSS_RSA_METHOD*/

#ifndef E_NSS_RSA_METHOD
typedef int e_nss_rsa_empty_translation_unit;
#endif
