/**
 * NSS Engine - EVP keys
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2011-2021 Roumen Petrov
 */

static EVP_PKEY*
nss_load_key(ENGINE *e, const char *key_id, int private, NSS_UI *wincx) {
    NSS_CTX          *ctx;
    CERTCertificate  *cert = NULL;
    SECKEYPrivateKey *pvtkey = NULL;
    SECKEYPublicKey  *pubkey = NULL;
    EVP_PKEY         *pkey = NULL;
    NSS_KEYCTX       *keyctx = NULL;
    int               keytype;

    ctx = ENGINE_get_NSS_CTX(e);
    nss_verbose(ctx, "Loading %s key - '%s'.\n",
      (private ? "private" : "public"), key_id);

    if (!NSS_INIT_DATABASE(NSS_F_LOAD_KEY, ctx)) goto done;

    cert = PK11_FindCertFromNickname(key_id, NULL);
    if (cert == NULL) {
         NSSerr(NSS_F_LOAD_KEY, NSS_R_MISSING_CERT);
         goto done;
    }
    nss_debug(ctx, "Signer's certificate found.\n");

    if (private) {
        nss_trace(ctx, "wincx=%p\n", (void*)wincx);

        pvtkey = PK11_FindKeyByAnyCert(cert, wincx);
        nss_trace(ctx, "pvtkey=%p\n", (void*)pvtkey);
        if (pvtkey == NULL) {
            NSSerr(NSS_F_LOAD_KEY, NSS_R_MISSING_PVTKEY);
            goto done;
        }
    }

    pubkey = CERT_ExtractPublicKey(cert);
    nss_trace(ctx, "pubkey=%p\n", (void*)pubkey);
    if (pubkey == NULL) {
        NSSerr(NSS_F_LOAD_KEY, NSS_R_MISSING_PUBKEY);
        goto done;
    }

{
    SECItem* si_pubkey = PK11_DEREncodePublicKey(pubkey);

    if (si_pubkey == NULL) {
        NSSerr(NSS_F_LOAD_KEY, NSS_R_DERENCODE_PUBKEY);
        goto pubkeydone;
    }

    if (si_pubkey->type != siBuffer ) {
        NSSerr(NSS_F_LOAD_KEY, NSS_R_DERENCODE_PUBKEYBUF);
        goto pubkeydone;
    }

{   /* decode public key */
    long l = si_pubkey->len;
    void *q;

    q = OPENSSL_malloc(l);
    if (q == NULL) {
        NSSerr(NSS_F_LOAD_KEY, NSS_R_INSUFFICIENT_MEMORY);
        goto pubkeydone;
    }

    memcpy(q, si_pubkey->data, l);
    nss_trace(ctx, "q=%p\n", (void*)q);

{   /* use another pointer to avoid crash ;) */
#if defined(OPENSSL_VERSION_NUMBER) && (OPENSSL_VERSION_NUMBER < 0x00908000L)
    unsigned char *p = (unsigned char *)q;
#else
    const unsigned char *p = q;
#endif
    pkey = d2i_PUBKEY(NULL, &p, l);
    keytype = nss_EVP_PKEY_type(pkey);
}
    nss_trace(ctx, "pkey=%p\n", (void*)pkey);

    OPENSSL_free(q);
}   /* public key decoded */

pubkeydone:
    if (si_pubkey)
        SECITEM_FreeItem(si_pubkey, PR_TRUE);
}

    if (pkey == NULL) goto done;

    /* avoid errors as first check for key type */
    switch (keytype) {
    case EVP_PKEY_RSA:
        keyctx = nss_init_keyctx_rsa(pkey, e);
        break;

#ifdef E_NSS_DSA_METHOD
    case EVP_PKEY_DSA:
        keyctx = nss_init_keyctx_dsa(pkey, e);
        break;
#endif /*def E_NSS_DSA_METHOD*/

#ifdef E_NSS_EC_METHOD
    case EVP_PKEY_EC:
        keyctx = nss_init_keyctx_ec(pkey, e);
        break;
#endif /*def E_NSS_EC_METHOD*/

#ifdef E_NSS_ECDSA_METHOD
    case EVP_PKEY_EC:
        keyctx = nss_init_keyctx_ec(pkey, e);
        break;
#endif /*def E_NSS_ECDSA_METHOD*/

    default: {
        NSSerr(NSS_F_LOAD_KEY, NSS_R_UNSUPPORTED_KEYTYPE);
        { /* add extra error message data */
            char msgstr[10];
            BIO_snprintf(msgstr, sizeof(msgstr), "%d", keytype);
            ERR_add_error_data(2, "KEYTYPE=", msgstr);
        }
        EVP_PKEY_free(pkey);
        pkey = NULL;
        } break;
    }

    /* update XXX key context*/
    if (keyctx == NULL) {
        NSSerr(NSS_F_LOAD_KEY, NSS_R_MISSING_KEY_CONTEXT);
        EVP_PKEY_free(pkey);
        pkey = NULL;
        goto done;
    }

    nss_trace(ctx, "keyctx=%p\n", (void*)keyctx);
    keyctx->cert = cert;
    cert = NULL;
    keyctx->pvtkey = pvtkey;
    nss_trace(ctx, "keyctx->pvtkey=%p\n", (void*)keyctx->pvtkey);
    pvtkey = NULL;
    keyctx->pubkey = pubkey;
    nss_trace(ctx, "keyctx->pubkey=%p\n", (void*)keyctx->pubkey);
    pubkey = NULL;

done:
    if (pubkey) {
        SECKEY_DestroyPublicKey(pubkey);
        pubkey = NULL;
    }
    if (pvtkey) {
        SECKEY_DestroyPrivateKey(pvtkey);
        pvtkey = NULL;
    }
    if (cert) {
        CERT_DestroyCertificate(cert);
        cert = NULL;
    }
    nss_trace(ctx, "^ pkey=%p\n", (void*)pkey);
    return pkey;
}


EVP_PKEY*
nss_load_privkey_c(ENGINE *e, const char *key_id, const UI_METHOD *ui_method, void *ui_data) {
    NSS_UI wincx = { ui_method, ui_data };

    return nss_load_key(e, key_id, 1, &wincx);
}


EVP_PKEY*
nss_load_pubkey_c(ENGINE *e, const char *key_id, const UI_METHOD *ui_method, void *ui_data) {
    NSS_UI wincx = { ui_method, ui_data };

    return nss_load_key(e, key_id, 0, &wincx);
}


/* NOTE engine load key functions are declared with non-const UI method! */
static EVP_PKEY*
nss_load_privkey(ENGINE *e, const char *key_id, UI_METHOD *ui_method, void *ui_data) {
    return nss_load_privkey_c(e, key_id, ui_method, ui_data);
}


static EVP_PKEY*
nss_load_pubkey(ENGINE *e, const char *key_id, UI_METHOD *ui_method, void *ui_data) {
    return nss_load_pubkey_c(e, key_id, ui_method, ui_data);
}
