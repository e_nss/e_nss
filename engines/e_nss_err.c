/**
 * NSS Engine - errors
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2011-2021 Roumen Petrov
 */

#include "e_nss_err.h"

#ifndef OPENSSL_NO_ERR

#define ERR_FUNC(func)		ERR_PACK(0, func, 0)

static ERR_STRING_DATA
NSS_str_functs[] = {
    { ERR_FUNC(NSS_F_INIT)		, "INIT" },
    { ERR_FUNC(NSS_F_FINISH)		, "FINISH" },
    { ERR_FUNC(NSS_F_CMD_CONFIG_DIR)	, "CMD_CONFIG_DIR" },
    { ERR_FUNC(NSS_F_CMD_LIST_CERT)	, "CMD_LIST_CERT" },
    { ERR_FUNC(NSS_F_CMD_EVP_CERT)	, "CMD_EVP_CERT" },
    { ERR_FUNC(NSS_F_STORE_OPEN)	, "STORE_OPEN" },
    { ERR_FUNC(NSS_F_LOAD_KEY)		, "LOAD_KEY" },
    { ERR_FUNC(NSS_F_GET_CERT)		, "GET_CERT" },
    { ERR_FUNC(NSS_F_INIT_KEYCTX)	, "NSS_INIT_KEYCTX" },
    { ERR_FUNC(NSS_F_RSA_INIT)		, "RSA_INIT" },
    { ERR_FUNC(NSS_F_RSA_PRIV_DEC)	, "RSA_PRIV_DEC" },
    { ERR_FUNC(NSS_F_RSA_PRIV_ENC)	, "RSA_PRIV_ENC" },
    { ERR_FUNC(NSS_F_RSA_SIGN)		, "RSA_SIGN" },
    { ERR_FUNC(NSS_F_RSA_VERIFY)	, "RSA_VERIFY" },
    { ERR_FUNC(NSS_F_DSA_INIT)		, "DSA_INIT" },
    { ERR_FUNC(NSS_F_DSA_DO_SIGN)	, "DSA_DO_SIGN" },
    { ERR_FUNC(NSS_F_ECDSA_DO_SIGN)	, "ECDSA_DO_SIGN" },
    { ERR_FUNC(NSS_F_ECDSA_DO_VERIFY)	, "ECDSA_DO_VERIFY" },
    { ERR_FUNC(NSS_F_EC_INIT)		, "EC_INIT" },
    {0,  NULL}
};


#define ERR_REASON(reason)	ERR_PACK(0, 0, reason)

static ERR_STRING_DATA
NSS_str_reasons[] = {
    { ERR_REASON(NSS_R_ENG_CTX_INDEX)		, "Engine context index" },
    { ERR_REASON(NSS_R_DB_IS_NOT_INITIALIZED)	, "DB is not initialized" },
    { ERR_REASON(NSS_R_CANNOT_SETUP_CONFIG_DIR)	, "Cannot setup config dir" },
    { ERR_REASON(NSS_R_CONFIG_DIR_IS_SET)	, "Config dir is set" },
    { ERR_REASON(NSS_R_NOT_IN_FIPS_MODE)	, "Not in FIPS mode" },
    { ERR_REASON(NSS_R_SHUTDOWN_FAIL)		, "Shutdown fail" },

    { ERR_REASON(NSS_R_INVALID_ARGUMENT)	, "Invalid argument" },
    { ERR_REASON(NSS_R_INSUFFICIENT_MEMORY)	, "Insufficient memory" },
    { ERR_REASON(NSS_R_NOT_SUPPORTED)		, "Not supported" },
    { ERR_REASON(NSS_R_INVALID_ALGORITHM)	, "Invalid algorithm" },
    { ERR_REASON(NSS_R_UNSUPPORTED_KEYTYPE)	, "Unsupported key type" },
    { ERR_REASON(NSS_R_UNSUPPORTED_NID)		, "Unsupported NID" },
    { ERR_REASON(NSS_R_UNSUPPORTED_PADDING)	, "Unsupported padding" },
    { ERR_REASON(NSS_R_DISABLED_ALGORITHM)	, "Disabled algorithm" },

    { ERR_REASON(NSS_R_MISSING_KEY_CONTEXT)	, "Missing key context" },
    { ERR_REASON(NSS_R_MISSING_CERT)		, "Missing certificate" },
    { ERR_REASON(NSS_R_MISSING_PUBKEY)		, "Missing public key" },
    { ERR_REASON(NSS_R_MISSING_PVTKEY)		, "Missing private key" },
    { ERR_REASON(NSS_R_DERENCODE_PUBKEY)	, "Derencode pubkey" },
    { ERR_REASON(NSS_R_DERENCODE_PUBKEYBUF)	, "Derencode pubkeybuf" },

    { ERR_REASON(NSS_R_DECRYPT_FAIL)		, "Decrypt fail" },
    { ERR_REASON(NSS_R_SGN_DIGEST_FAIL)		, "SGN_Digest fail" },
    { ERR_REASON(NSS_R_VERIFY_DIGEST_FAIL)	, "VFY_VerifyDigest fail" },

    { ERR_REASON(NSS_R_BAD_SIGNATURE)		, "Bad signature" },
    { ERR_REASON(NSS_R_INVALID_DIGEST_LENGTH)	, "Invalid digest length" },
    { ERR_REASON(NSS_R_INVALID_SIGNATURE_LENGTH), "Invalid signature length" },
    { 0, NULL}
};


static ERR_STRING_DATA
NSS_lib_name[] = {
   {0, "E_NSS" },
   {0, NULL}
};

#endif /*ndef OPENSSL_NO_ERR*/


static int NSS_lib_error_code = 0;
static int NSS_error_init = 1;

static void
ERR_load_NSS_strings(void) {
    if (NSS_lib_error_code == 0)
        NSS_lib_error_code = ERR_get_next_error_library();

    if (NSS_error_init == 0) return;
    NSS_error_init = 0;

#ifndef OPENSSL_NO_ERR
    ERR_load_strings(NSS_lib_error_code, NSS_str_functs);
    ERR_load_strings(NSS_lib_error_code, NSS_str_reasons);

    NSS_lib_name->error = ERR_PACK(NSS_lib_error_code, 0, 0);
    ERR_load_strings(0, NSS_lib_name);
#endif /*ndef OPENSSL_NO_ERR*/
}

static void
ERR_unload_NSS_strings(void) {
    if (NSS_error_init == 1) return;
    NSS_error_init = 1;

#ifndef OPENSSL_NO_ERR
    ERR_unload_strings(NSS_lib_error_code, NSS_str_functs);
    ERR_unload_strings(NSS_lib_error_code, NSS_str_reasons);

    ERR_unload_strings(0, NSS_lib_name);
#endif /*ndef OPENSSL_NO_ERR*/
}

void
ERR_NSS_error(
    const char *file, int line, const char *funcname,
    int function, int reason
) {
    if (NSS_lib_error_code == 0)
        NSS_lib_error_code = ERR_get_next_error_library();

#ifdef OPENSSL_NO_FILENAMES /* OpenSSL 1.1+ */
    file = NULL;
    line = 0;
#endif
#ifdef ERR_raise_data /* OpenSSL 3.0+ */
    UNUSED(function);
    ERR_new();
    ERR_set_debug(file, line, funcname);
    ERR_set_error(NSS_lib_error_code, reason, NULL);
#else
#  ifdef OPENSSL_NO_ERR
    /* If ERR_PUT_error macro ignores file and line */
    UNUSED(file);
    UNUSED(line);
#  endif
    UNUSED(funcname);
    ERR_PUT_error(NSS_lib_error_code, function, reason, file, line);
#endif
}
