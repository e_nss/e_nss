/**
 * NSS Engine - EC or ECDSA method
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2015-2023 Roumen Petrov
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#ifndef USE_OPENSSL_PROVIDER
/* TODO: implement OpenSSL 4.0 API, as OpenSSL 3.* is quite nonfunctional */
# define OPENSSL_SUPPRESS_DEPRECATED
#endif

#if defined(E_NSS_EC_METHOD) || defined(E_NSS_ECDSA_METHOD)
#define OPENSSL_OCSP_H /* see e_nss_store.c */
#include <openssl/engine.h>

#include "e_nss_int.h"
#include "e_nss_err.h"

#include <nss.h>
#include <cryptohi.h>
#include <secerr.h>
/* does not exist in "old" versions */
#define SEC_ERROR_SIGNATURE_ALGORITHM_DISABLED	(SEC_ERROR_BASE + 181)


#ifndef HAVE_EC_KEY_GET0_ENGINE
/* OpenSSL capsulate EC key structures and APIs before 1.1.1 does not
 * offer access to engine associated to key. As work-around bind method
 * stores engine to a static variable.
 * NOTE: bind metod is called more then once :( !
 */
static ENGINE *e_nss_ec_engine = NULL;

static inline ENGINE*
EC_KEY_get0_engine(const EC_KEY *eckey) {
    /*work-around implementation*/
    (void)eckey;
    return e_nss_ec_engine;
}
#endif /*ndef HAVE_EC_KEY_GET0_ENGINE*/

#ifndef HAVE_DSA_SIG_SET0
/* OpenSSL >= 1.1 by commits "Implement DSA_SIG_set0() and
 * ECDSA_SIG_set0(), for setting signature values." and etc.
 */
static inline int/*bool*/
ECDSA_SIG_set0(ECDSA_SIG *sig, BIGNUM *r, BIGNUM *s) {
    if (r == NULL || s == NULL) return 0;

    BN_clear_free(sig->r);
    BN_clear_free(sig->s);
    sig->r = r;
    sig->s = s;
    return 1;
}
#endif /*ndef HAVE_DSA_SIG_SET0*/


/* ===== NSS EC/ECDSA method ===== */

static int nss_ec_ctx_index = -1;


static inline NSS_KEYCTX*
nss_get_keyctx(EC_KEY *key) {
#ifdef E_NSS_EC_METHOD
    return EC_KEY_get_ex_data(key, nss_ec_ctx_index);
#else
    return ECDSA_get_ex_data(key, nss_ec_ctx_index);
#endif
}

NSS_KEYCTX*
nss_get_keyctx_ec(EVP_PKEY *pkey) {
    NSS_KEYCTX *ret;
{/* OpenSSL >= 1.1 by commit "Add EVP_PKEY_get0_* functions."
    adds EVP_PKEY_get0_EC_KEY with not constant return value.
    In 3.0 definition is changed to return constant!
    So throw away get0 and use existing since 0.9.6f get1 function
    with non significant performance penalty.
  */
    EC_KEY *ec = EVP_PKEY_get1_EC_KEY(pkey);
    ret = nss_get_keyctx(ec);
    EC_KEY_free(ec);
}
    return ret;
}

#ifdef E_NSS_ECDSA_METHOD
static inline int/*bool*/
nss_ec_set_keyctx(EC_KEY *key, NSS_KEYCTX *keyctx) {
    return ECDSA_set_ex_data(key, nss_ec_ctx_index, keyctx);
}
#endif


static void
NSS_KEYCTX_free_ec(
    void *parent, void *ptr, CRYPTO_EX_DATA *ad,
    int idx, long argl, void *argp
) {
    UNUSED(parent); UNUSED(ad); UNUSED(argl); UNUSED(argp);

    CALL_TRACE("idx: %d, ptr: %p\n", idx, ptr);
    if (idx == nss_ec_ctx_index)
         NSS_KEYCTX_free(ptr);
}


static inline NSS_CTX*
EC_KEY_get_NSS_CTX(const EC_KEY *key) {
    const ENGINE *e = EC_KEY_get0_engine(key);
    return ENGINE_get_NSS_CTX(e);
}


static ECDSA_SIG*
nss_ecdsa_do_sign(
    const unsigned char *dgst, int dgst_len,
    const BIGNUM *inv, const BIGNUM *rp, EC_KEY *eckey
);

static int
nss_ecdsa_sign_setup(
    EC_KEY *eckey, BN_CTX *ctx, BIGNUM **kinv, BIGNUM **r
);

static int
nss_ecdsa_do_verify(
    const unsigned char *dgst, int dlen, const ECDSA_SIG *sig, EC_KEY *eckey
);


static int /*bool*/
nss_ecdsa_sign_setup(
    EC_KEY *eckey, BN_CTX *ctx, BIGNUM **kinvp, BIGNUM **rp
) {
    CALL_TRACE("eckey=%p\n", (void*)eckey);

    UNUSED(ctx);
    *kinvp = NULL;
    *rp = NULL;
    return 1;
}


static ECDSA_SIG*
nss_ecdsa_do_sign(
    const unsigned char *dgst, int dlen,
    const BIGNUM *inv, const BIGNUM *rp,
    EC_KEY *eckey
) {
    ECDSA_SIG*  ret = NULL;
    SECStatus   rv;
    NSS_CTX    *ctx;
    NSS_KEYCTX *keyctx;

    ctx = EC_KEY_get_NSS_CTX(eckey);
    nss_trace(ctx, "args: dlen=%d\n", dlen);
    nss_trace(ctx, "eckey=%p, inv=%p, rp=%p\n", (void*)eckey, (void*)inv, (void*)rp);

    keyctx = nss_get_keyctx(eckey);
    nss_trace(ctx, "keyctx=%p\n", (void*)keyctx);
    if (keyctx == NULL) {
        NSSerr(NSS_F_ECDSA_DO_SIGN, NSS_R_MISSING_KEY_CONTEXT);
        goto done;
    }

    nss_trace(ctx, "keyctx->pvtkey=%p\n", (void*)keyctx->pvtkey);
    if (keyctx->pvtkey == NULL) {
        NSSerr(NSS_F_ECDSA_DO_SIGN, NSS_R_MISSING_PVTKEY);
        goto done;
    }

    nss_trace(ctx, "keyctx->pvtkey->keyType=%d\n", keyctx->pvtkey->keyType);

{   /* compute nss signature */
    SECItem    digest;
    SECItem    result;
    SECOidTag  hashalg = SEC_OID_UNKNOWN /*need for RSA only*/;

    digest.type = siBuffer;
    digest.data = (unsigned char*)dgst;
    digest.len  = dlen;

    result.type = siBuffer;
    result.data = NULL;
    result.len  = 0;

    rv = SGN_Digest(keyctx->pvtkey, hashalg, &result, &digest);
    nss_trace(ctx, "rv=%d\n", rv);
    if (rv != SECSuccess) {
        int port_err = PORT_GetError();
        switch(port_err) {
        case SEC_ERROR_INVALID_ALGORITHM: {
            NSSerr(NSS_F_ECDSA_DO_SIGN, NSS_R_INVALID_ALGORITHM);
            } break;
        case SEC_ERROR_SIGNATURE_ALGORITHM_DISABLED: {
            NSSerr(NSS_F_ECDSA_DO_SIGN, NSS_R_DISABLED_ALGORITHM);
            } break;
        default: {
            int port_err_off = port_err - SEC_ERROR_BASE;

            nss_trace(ctx, "port_err/ofset=%d/%d\n", port_err, port_err_off);
            NSSerr(NSS_F_ECDSA_DO_SIGN, NSS_R_SGN_DIGEST_FAIL);
            {/*add extra error message data*/
                char msgstr[10];
                BIO_snprintf(msgstr, sizeof(msgstr), "%d", port_err_off);
                ERR_add_error_data(2, "PORT_ERROR_OFFSET=", msgstr);
            }
            } break;
        }
        goto done_sign;
    }


    /* propagate result */
    nss_trace(ctx, "result.len=%d\n", result.len);

    ret = ECDSA_SIG_new();
    if (ret == NULL) {
        NSSerr(NSS_F_ECDSA_DO_SIGN, NSS_R_INSUFFICIENT_MEMORY);
        goto done_sign;
    }

{   /* decode signature */
    int k = result.len >> 1; /* len is even integer */
    BIGNUM *nr, *ns;
    BIGNUM *pr, *ps;

#ifdef E_NSS_EC_METHOD
    /* NOTE: Initially (OpenSSL 0.9.8) "ECDSA_SIG_new" alocates memory
     * for structure members r and s but in OpenSSL pre 1.1 this is
     * changed by commit: "Don't allocate r/s in DSA_SIG and ECDSA_SIG".
     */
    pr = ps = NULL;
#else
    pr = ret->r;
    ps = ret->s;
#endif

    nr = BN_bin2bn(result.data  , k, pr);
    ns = BN_bin2bn(result.data+k, k, ps);

    if ((nr == NULL) || (ns == NULL)) {
        NSSerr(NSS_F_ECDSA_DO_SIGN, NSS_R_INSUFFICIENT_MEMORY);
        ECDSA_SIG_free(ret);
        ret = NULL;
        goto done_sign;
    }
    if ((pr == NULL) && !ECDSA_SIG_set0(ret, nr, ns)) { /* OpenSSL 1.1+ */
        NSSerr(NSS_F_ECDSA_DO_SIGN, NSS_R_INSUFFICIENT_MEMORY);
        BN_free(nr);
        BN_free(ns);
        ECDSA_SIG_free(ret);
        ret = NULL;
    }
}

done_sign:
    if (result.data != NULL) {
        PORT_Free(result.data);
    }
}   /* end of nss signature computation */

done:
    return ret;
}


#ifdef E_NSS_EC_METHOD
static int
nss_ecdsa_sign(int type,
    const unsigned char *dgst, int dlen,
    unsigned char *sig, unsigned int *siglen,
    const BIGNUM *inv, const BIGNUM *rp,
    EC_KEY *eckey
) {
    ECDSA_SIG *s;

    UNUSED(type);

    s = nss_ecdsa_do_sign(dgst, dlen, inv, rp, eckey);
    if (s == NULL) {
        *siglen = 0;
        return 0;
    }

    *siglen = i2d_ECDSA_SIG(s, &sig);

    ECDSA_SIG_free(s);
    return 1;
}
#endif /* def E_NSS_EC_METHOD */


static int
nss_ecdsa_do_verify(
    const unsigned char *dgst, int dlen, const ECDSA_SIG *sig, EC_KEY *eckey
) {
    int         ret = -1;
    SECStatus   rv;
    NSS_CTX    *ctx;
    NSS_KEYCTX *keyctx;

    ctx = EC_KEY_get_NSS_CTX(eckey);
    nss_trace(ctx, "args: dlen=%d\n", dlen);

    keyctx = nss_get_keyctx(eckey);
    nss_trace(ctx, "keyctx=%p\n", (void*)keyctx);
    if (keyctx == NULL) {
        NSSerr(NSS_F_ECDSA_DO_VERIFY, NSS_R_MISSING_KEY_CONTEXT);
        goto done;
    }

    nss_trace(ctx, "keyctx->pubkey=%p\n", (void*)keyctx->pubkey);
    if (keyctx->pubkey == NULL) {
        NSSerr(NSS_F_ECDSA_DO_VERIFY, NSS_R_MISSING_PUBKEY);
        goto done;
    }

    nss_trace(ctx, "keyctx->pubkey->keyType=%d\n", keyctx->pubkey->keyType);

{   /* nss signature verification */
    SECItem    digest;
    SECItem    signature;
    SECOidTag  algid = SEC_OID_ANSIX962_ECDSA_SIGNATURE_RECOMMENDED_DIGEST;

    digest.type = siBuffer;
    digest.data = (unsigned char *)dgst;
    digest.len  = dlen;

{   /* encode ECDSA signature */
    unsigned char *sigbuf;
    int siglen;

    siglen = i2d_ECDSA_SIG(sig, NULL);
    if (siglen <= 0) goto done;

    sigbuf  = OPENSSL_malloc(siglen);
    if (sigbuf == NULL) goto done;

{   unsigned char *pbuf = sigbuf;
    if (siglen != i2d_ECDSA_SIG(sig, &pbuf)) {
        OPENSSL_free(sigbuf);
        goto done;
    }
}

    signature.type = siBuffer;
    signature.data = sigbuf;
    signature.len  = siglen;
}   /* end of ECDSA signature encoding */

    ret = 0;

{   NSS_UI wincx = { UI_get_default_method(), NULL };
    rv = VFY_VerifyDigest(&digest, keyctx->pubkey, &signature, algid, &wincx);
}
    OPENSSL_free(signature.data);
    if (rv != SECSuccess) {
        int port_err = PORT_GetError();

        switch(port_err) {
        case SEC_ERROR_BAD_SIGNATURE: {
            NSSerr(NSS_F_ECDSA_DO_VERIFY, NSS_R_BAD_SIGNATURE);
            } break;
        case SEC_ERROR_INVALID_ALGORITHM: {
            NSSerr(NSS_F_ECDSA_DO_VERIFY, NSS_R_INVALID_ALGORITHM);
            } break;
        case SEC_ERROR_SIGNATURE_ALGORITHM_DISABLED: {
            NSSerr(NSS_F_ECDSA_DO_VERIFY, NSS_R_DISABLED_ALGORITHM);
            } break;
        default: {
            int port_err_off = port_err - SEC_ERROR_BASE;

            nss_trace(ctx, "port_err/ofset=%d/%d\n", port_err, port_err_off);
            NSSerr(NSS_F_ECDSA_DO_VERIFY, NSS_R_VERIFY_DIGEST_FAIL);
            {/*add extra error message data*/
                char msgstr[10];
                BIO_snprintf(msgstr, sizeof(msgstr), "%d", port_err_off);
                ERR_add_error_data(2, "PORT_ERROR_OFFSET=", msgstr);
            }
            } break;
        }
        goto done;
    }
}   /* end of nss signature verification */

    ret = 1;
done:
    nss_trace(ctx, "ret=%d\n", ret);
    return ret;
}


#ifdef E_NSS_EC_METHOD
static int
nss_ecdsa_verify(int type,
    const unsigned char *dgst, int dgst_len,
    const unsigned char *sigbuf, int sig_len,
    EC_KEY *eckey
) {
    int        ret = -1;
    ECDSA_SIG *sig;

    UNUSED(type);

    sig = ECDSA_SIG_new();
    if (sig == NULL) goto done;

{   const unsigned char *p = sigbuf;
    if (d2i_ECDSA_SIG(&sig, &p, sig_len) == NULL)
        goto done;
}

#if 0
/* FIXME: crash in some applications */
{ /* check trailing garbage */
    int derlen;
    unsigned char *der;

    derlen = i2d_ECDSA_SIG(sig, &der);
    if ((derlen != sig_len) || (memcmp(sigbuf, der, derlen) != 0)) {
        OPENSSL_clear_free(der, derlen);
        goto done;
    }
    OPENSSL_clear_free(der, derlen);
}
#endif

    ret = nss_ecdsa_do_verify(dgst, dgst_len, sig, eckey);

done:
    ECDSA_SIG_free(sig);
    return ret;
}
#endif /* def E_NSS_EC_METHOD */

#endif /* defined(E_NSS_EC_METHOD) || defined(E_NSS_ECDSA_METHOD) */


#ifdef E_NSS_ECDSA_METHOD

#ifndef HAVE_ECDSA_METHOD_NEW

/* Structure ECDSA_METHOD is not public!
 * If method ECDSA_METHOD_new(OpenSSL 1.0.2+) is not defined, for
 * compatibility, we will define structure to simulate methods that
 * set its attributes.
 */
struct ecdsa_method {
    const char *name;
    ECDSA_SIG *(*ecdsa_do_sign) (const unsigned char *dgst, int dgst_len,
                                 const BIGNUM *inv, const BIGNUM *rp,
                                 EC_KEY *eckey);
    int (*ecdsa_sign_setup) (EC_KEY *eckey, BN_CTX *ctx, BIGNUM **kinv,
                             BIGNUM **r);
    int (*ecdsa_do_verify) (const unsigned char *dgst, int dgst_len,
                            const ECDSA_SIG *sig, EC_KEY *eckey);
# if 0
    int (*init) (EC_KEY *eckey);
    int (*finish) (EC_KEY *eckey);
# endif
    int flags;
    void *app_data;
};


static ECDSA_METHOD*
ECDSA_METHOD_new(ECDSA_METHOD *ecdsa_method)
{
    UNUSED(ecdsa_method);
    return OPENSSL_malloc(sizeof(ECDSA_METHOD));
}


void
ECDSA_METHOD_free(ECDSA_METHOD *ecdsa_method) {
    OPENSSL_free(ecdsa_method);
}


static void
ECDSA_METHOD_set_sign(
    ECDSA_METHOD *ecdsa_method,
    ECDSA_SIG *(*ecdsa_do_sign) (
        const unsigned char *dgst, int dgst_len,
        const BIGNUM *inv, const BIGNUM *rp, EC_KEY *eckey)
) {
    ecdsa_method->ecdsa_do_sign = ecdsa_do_sign;
}


static void
ECDSA_METHOD_set_sign_setup(
    ECDSA_METHOD *ecdsa_method,
    int (*ecdsa_sign_setup) (
        EC_KEY *eckey, BN_CTX *ctx, BIGNUM **kinv, BIGNUM **r)
) {
    ecdsa_method->ecdsa_sign_setup = ecdsa_sign_setup;
}


static void
ECDSA_METHOD_set_verify(
    ECDSA_METHOD *ecdsa_method,
    int (*ecdsa_do_verify) (
        const unsigned char *dgst, int dgst_len,
        const ECDSA_SIG *sig, EC_KEY *eckey)
) {
    ecdsa_method->ecdsa_do_verify = ecdsa_do_verify;
}


static void
ECDSA_METHOD_set_flags(ECDSA_METHOD *ecdsa_method, int flags)
{
    ecdsa_method->flags = flags;
}


static void
ECDSA_METHOD_set_name(ECDSA_METHOD *ecdsa_method, char *name)
{
    ecdsa_method->name = name;
}

#endif /*ndef HAVE_ECDSA_METHOD_NEW*/


NSS_KEYCTX*
nss_init_keyctx_ec(EVP_PKEY *pkey, ENGINE *e) {
    NSS_KEYCTX *keyctx;
    EC_KEY *pkey_ec;

    pkey_ec = EVP_PKEY_get1_EC_KEY(pkey);
    CALL_TRACE("pkey=%p, key=%p\n", (void*)pkey, (void*)pkey_ec);

    /* always associate ec method with engine ;) */
    ECDSA_set_method(pkey_ec, ENGINE_get_ECDSA(e));

    keyctx = nss_get_keyctx(pkey_ec);
    CALL_TRACE("keyctx: %p\n", (void*)keyctx);
    if (keyctx == NULL)
        keyctx = NSS_KEYCTX_new();
    CALL_TRACE("idx: %d, ptr: %p\n", nss_ec_ctx_index, (void*)keyctx);
    if (keyctx == NULL) {
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INSUFFICIENT_MEMORY);
        goto err;
    }
    if (!nss_ec_set_keyctx(pkey_ec, keyctx)) {
        NSS_KEYCTX_free(keyctx);
        keyctx = NULL;
    }

err:
    EC_KEY_free(pkey_ec);
    return keyctx;
}


#ifdef HAVE_FIPS_MODE
/* Unfortunately ECDSA_FLAG_FIPS_METHOD is defined in non
 * distributed(local) header ecdsa/ecs_locl.h.
 * To avoid definition on non-FIPS capable OpenSSL versions(1.0.0*),
 * we define it only if is available function FIPS_mode.
 */
# ifndef ECDSA_FLAG_FIPS_METHOD
#  define ECDSA_FLAG_FIPS_METHOD 0x1
# endif
#endif

int/*bool*/
bind_nss_ecdsa_method(ENGINE *e) {
    ECDSA_METHOD *ecdsa_method = ECDSA_METHOD_new(NULL);

    CALL_TRACE("ecdsa_method=%p\n", (void*)ecdsa_method);
    /* if out of memory */
    if (ecdsa_method == NULL) return 0;

    /* ensure ECDSA context index */
    if (nss_ec_ctx_index < 0)
        nss_ec_ctx_index = ECDSA_get_ex_new_index(0,
            NULL, NULL, NULL, NSS_KEYCTX_free_ec);

    CALL_TRACE("nss_ec_ctx_index=%d\n", nss_ec_ctx_index);
    if (nss_ec_ctx_index < 0) goto err;

    ECDSA_METHOD_set_name(ecdsa_method, (char*)"NSS ECDSA method");
    ECDSA_METHOD_set_sign(ecdsa_method, nss_ecdsa_do_sign);
    ECDSA_METHOD_set_sign_setup(ecdsa_method, nss_ecdsa_sign_setup);
    ECDSA_METHOD_set_verify(ecdsa_method, nss_ecdsa_do_verify);
    ECDSA_METHOD_set_flags(ecdsa_method,
    #ifdef ECDSA_FLAG_FIPS_METHOD
        ECDSA_FLAG_FIPS_METHOD |
    #endif
        0
    );

    if (ENGINE_set_ECDSA(e, ecdsa_method)) {
        /* NOTE OpenSSL API < 1.1.0 here */
        e_nss_ec_engine = e;
        return 1;
    }

err:
    ECDSA_METHOD_free(ecdsa_method);
    return 0;
}


void
destroy_nss_ecdsa_method(ENGINE *e) {
    ECDSA_METHOD *ecdsa_method;

    ecdsa_method = (ECDSA_METHOD*) ENGINE_get_ECDSA(e);
    ENGINE_set_ECDSA(e, NULL);

    ECDSA_METHOD_free(ecdsa_method);
}
#endif /*def E_NSS_ECDSA_METHOD*/

#ifdef E_NSS_EC_METHOD

static int
nss_ec_init(EC_KEY *key) {
    int ret = 0;

    CALL_TRACE("key=%p\n", (void*)key);

{   /* setup NSS EC key context */
    NSS_KEYCTX *keyctx = NSS_KEYCTX_new();
    CALL_TRACE("idx=%d, keyctx=%p\n", nss_ec_ctx_index, (void*)keyctx);
    if (keyctx == NULL) {
        NSSerr(NSS_F_EC_INIT, NSS_R_INSUFFICIENT_MEMORY);
        goto done;
    }
    EC_KEY_set_ex_data(key, nss_ec_ctx_index, keyctx);
}

    ret = 1;
done:
    CALL_TRACE("^ %s\n", (ret ? "ok": ""));
    return ret;
}


NSS_KEYCTX*
nss_init_keyctx_ec(EVP_PKEY *pkey, ENGINE *e) {
    NSS_KEYCTX *ret = NULL;
    EC_KEY *ec;
    EC_KEY *pkey_ec;

    pkey_ec = EVP_PKEY_get1_EC_KEY(pkey);
    CALL_TRACE("pkey=%p, key=%p\n", (void*)pkey, (void*)pkey_ec);
#ifdef HAVE_EVP_KEYMGMT_GET0_PROVIDER	/* OpenSSL >= 3.0 */
    /* FIPS enabled OpenSSL 3+ fail to return "elementary" key */
    if (pkey_ec == NULL) {
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INVALID_ARGUMENT);
        return NULL;
    }
#endif

    /* always associate ec method with engine ;) */
    ec = EC_KEY_new_method(e);
    if (ec == NULL) {
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INSUFFICIENT_MEMORY);
        goto err;
    }

{   const EC_GROUP *p = EC_KEY_get0_group(pkey_ec);
    if (!EC_KEY_set_group(ec, p)) {
        /* EC_GROUP duplicate may fail */
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INSUFFICIENT_MEMORY);
        goto err;
    }
}
{   const EC_POINT *p = EC_KEY_get0_public_key(pkey_ec);
    if (!EC_KEY_set_public_key(ec, p)) {
        /* EC_POINT duplicate may fail */
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INSUFFICIENT_MEMORY);
        goto err;
    }
}

    if (EVP_PKEY_set1_EC_KEY(pkey, ec))
        ret = nss_get_keyctx(ec);

err:
    EC_KEY_free(pkey_ec);
    EC_KEY_free(ec);
    return ret;
}


int/*bool*/
bind_nss_ec_method(ENGINE *e) {
    EC_KEY_METHOD *ec_method = EC_KEY_METHOD_new(NULL);

    CALL_TRACE("ec_method=%p\n", (void*)ec_method);
    /* if out of memory */
    if (ec_method == NULL) return 0;

    /* ensure EC context index */
    if (nss_ec_ctx_index < 0)
        nss_ec_ctx_index = EC_KEY_get_ex_new_index(0,
            NULL, NULL, NULL, NSS_KEYCTX_free_ec);

    CALL_TRACE("nss_ec_ctx_index=%d\n", nss_ec_ctx_index);
    if (nss_ec_ctx_index < 0) goto err;

    EC_KEY_METHOD_set_init(ec_method,
        nss_ec_init,
        NULL /* int (*finish)(...) */,
        NULL /* int (*copy)(...) */,
        NULL /* int (*set_group)(...) */,
        NULL /* int (*set_private)(...) */,
        NULL /* int (*set_public)(...) */
    );
    EC_KEY_METHOD_set_sign(ec_method,
        nss_ecdsa_sign,
        nss_ecdsa_sign_setup,
        nss_ecdsa_do_sign);
    EC_KEY_METHOD_set_verify(ec_method,
        nss_ecdsa_verify,
        nss_ecdsa_do_verify);

    if (ENGINE_set_EC(e, ec_method)) {
    #ifndef HAVE_EC_KEY_GET0_ENGINE
        e_nss_ec_engine = e;
    #endif
        return 1;
    }

err:
    EC_KEY_METHOD_free(ec_method);
    return 0;
}


void
destroy_nss_ec_method(ENGINE *e) {
    EC_KEY_METHOD *ec_method;

    ec_method = (EC_KEY_METHOD*) ENGINE_get_EC(e);
    (void)ENGINE_set_EC(e, NULL);
    EC_KEY_METHOD_free(ec_method);
}
#endif /*def E_NSS_EC_METHOD*/

#if !defined(E_NSS_EC_METHOD) && !defined(E_NSS_ECDSA_METHOD)
typedef int e_nss_ec_empty_translation_unit;
#endif
