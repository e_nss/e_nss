#ifndef HEADER_NSS_INT_H
#define HEADER_NSS_INT_H
/**
 * NSS Engine - internals
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2011-2021 Roumen Petrov
 */

#include <pk11pub.h>
#include <openssl/ossl_typ.h>


#ifdef  __cplusplus
extern "C" {
#endif

/* ===== OpenSSL compatibility ports  ===== */

static inline char*
NSS_strdup(const char *s) {
#ifdef HAVE_BUF_STRDUP
/* Unlike OPENSSL_strdup (0.8.k+), BUF_strdup is defined in
 * all OpenSSL versions (SSLeay 0.8.1)
 * In 1.1.0 is replaced with macro to function OPENSSL_strdup.
 * Unfortunately macro is deprecated in OPENSSL 3.0.
 * So on OpenSSL before 1.1.0 we will use BUF_strdup
 * as is always available at run-time.
 */
    return BUF_strdup(s);
#else
    return OPENSSL_strdup(s);
#endif
}


static inline int
nss_EVP_PKEY_type(const EVP_PKEY *pkey) {
#if defined(HAVE_EVP_PKEY_GET_BASE_ID)	/* OpenSSL >= 3.0 */
    return EVP_PKEY_get_base_id(pkey);
#elif defined HAVE_EVP_PKEY_BASE_ID	/* OpenSSL >= 1.0 */
    return EVP_PKEY_base_id(pkey);
#else
    return EVP_PKEY_type(pkey->type/*=id*/);
#endif
}


/* ===== E_NSS internals ===== */

#define NSS_LOGLEVEL_VERBOSE	1
#define NSS_LOGLEVEL_DEBUG	2
#define NSS_LOGLEVEL_TRACE	3
#define NSS_LOGLEVEL_LAST	NSS_LOGLEVEL_TRACE


struct NSS_CTX_s {
    /* setup */
    const char       *config_dir;

    /* for internal use */
    int               debug_level;
    const char       *error_file;
};

typedef struct NSS_CTX_s NSS_CTX;


struct NSS_KEYCTX_s {
    CERTCertificate   *cert;
    SECKEYPublicKey   *pubkey;
    SECKEYPrivateKey  *pvtkey;
};

typedef struct NSS_KEYCTX_s NSS_KEYCTX;


struct NSS_UI_s {
    const UI_METHOD *ui_method;
    void *ui_data;
};

typedef struct NSS_UI_s NSS_UI;


#define UNUSED(a)       (void)a


NSS_KEYCTX* NSS_KEYCTX_new(void);
void NSS_KEYCTX_free(NSS_KEYCTX *keyctx);

NSS_CTX* ENGINE_get_NSS_CTX(const ENGINE *e);


int/*bool*/
nss_init_database(
    const char *file, int line, const char *funcname,
    int function, NSS_CTX *ctx);
#define NSS_INIT_DATABASE(f,ctx) \
    nss_init_database(__FILE__, __LINE__, __func__, (f), (ctx))


EVP_PKEY*
nss_load_privkey_c(ENGINE *e, const char *key_id, const UI_METHOD *ui_method, void *ui_data);

EVP_PKEY*
nss_load_pubkey_c(ENGINE *e, const char *key_id, const UI_METHOD *ui_method, void *ui_data);


#ifdef E_NSS_RSA_METHOD
NSS_KEYCTX* nss_init_keyctx_rsa(EVP_PKEY *pkey, ENGINE *e);
NSS_KEYCTX* nss_get_keyctx_rsa(EVP_PKEY *pkey);

int/*bool*/ bind_nss_rsa_method(ENGINE *e);
void destroy_nss_rsa_method(ENGINE *e);
#endif

#ifdef E_NSS_DSA_METHOD
NSS_KEYCTX* nss_init_keyctx_dsa(EVP_PKEY *pkey, ENGINE *e);
NSS_KEYCTX* nss_get_keyctx_dsa(EVP_PKEY *pkey);

int/*bool*/ bind_nss_dsa_method(ENGINE *e);
void destroy_nss_dsa_method(ENGINE *e);
#endif

#if defined(E_NSS_EC_METHOD) || defined(E_NSS_ECDSA_METHOD)
NSS_KEYCTX* nss_init_keyctx_ec(EVP_PKEY *pkey, ENGINE *e);
NSS_KEYCTX* nss_get_keyctx_ec(EVP_PKEY *pkey);
#endif

#ifdef E_NSS_EC_METHOD
int/*bool*/ bind_nss_ec_method(ENGINE *e);
void destroy_nss_ec_method(ENGINE *e);
#endif

#ifdef E_NSS_ECDSA_METHOD
int/*bool*/ bind_nss_ecdsa_method(ENGINE *e);
void destroy_nss_ecdsa_method(ENGINE *e);
#endif


#ifdef E_NSS_STORE_LOADER
int/*bool*/ bind_nss_store(ENGINE *e);
void destroy_nss_store(ENGINE *e);
int/*bool*/ nss_cmd_list_cert_store(BIO *out, long i);
#endif


#ifdef ENABLE_CALL_TRACE
void trace_calls(const char *func, const char *fmt, ...)
    __attribute__((format(printf, 2, 3)));
#else
static inline void
trace_calls(const char *func, const char *fmt, ...) {
    UNUSED(func); UNUSED(fmt);
}
#endif

#define CALL_TRACE(...)	trace_calls(__func__, __VA_ARGS__)


#ifdef ENABLE_CALL_TRACE
# undef ENABLE_NSS_TRACE
# define ENABLE_NSS_TRACE
#endif

#ifdef ENABLE_NSS_TRACE
void f_nss_trace(const char *func, NSS_CTX *ctx, const char *fmt, ...)
    __attribute__((format(printf, 3, 4)));
#else
static inline void
f_nss_trace(const char *func, NSS_CTX *ctx, const char *fmt, ...) {
    UNUSED(func); UNUSED(ctx); UNUSED(fmt);
}
#endif

#define nss_trace(ctx, ...)	f_nss_trace(__func__, ctx, __VA_ARGS__)


#ifdef  __cplusplus
}
#endif
#endif /*ndef HEADER_NSS_INT_H*/
