/**
 * NSS Engine - user interface
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2011-2022 Roumen Petrov
 */

#undef CLEAR_ERR_DSO_BIND_FUNC
#ifdef HAVE_EC_KEY_GET0_ENGINE		/* OpenSSL >= 1.1.1 */
# ifndef HAVE_EVP_PKEY_GET_BASE_ID	/* OpenSSL < 3.0 */
#  define CLEAR_ERR_DSO_BIND_FUNC
# endif
#endif

#ifdef CLEAR_ERR_DSO_BIND_FUNC
static void
clear_err_dso_bind_func(void) {
    unsigned long err_code;

/* Avoid post OpenSSL 1.1.1m error left in queue after load of engine.
 * It is due to a commit that try to correct silly load of incompatible engine:
 * ...:error:2506406A:DSO support routines:dlfcn_bind_func:could not bind to the requested symbol name:crypto/dso/dso_dlfcn.c:188:symname(EVP_PKEY_get_base_id): ../engines/.libs/e_nss.so: undefined symbol: EVP_PKEY_get_base_id
 * ...:error:2506C06A:DSO support routines:DSO_bind_func:could not bind to the requested symbol name:crypto/dso/dso_lib.c:186:
 * Note that function definitions are internal in OpenSSL 1.1.1.
 * Issue is that reason definitions are internal as well!
 */
#ifndef DSO_R_SYM_FAILURE
# define DSO_R_SYM_FAILURE 106/*0x6A*/
#endif
    /* 2506406A for instance from dlfnc interface method */
    err_code = ERR_peek_error();
    if (ERR_GET_LIB(err_code) != ERR_LIB_DSO /*37=0x25*/&&
        ERR_GET_REASON(err_code) != DSO_R_SYM_FAILURE &&
        (
        ERR_GET_FUNC(err_code) != 100 /*DSO_F_DLFCN_BIND_FUNC=0x64*/&&
        ERR_GET_FUNC(err_code) != 101 /*DSO_F_WIN32_BIND_FUNC=0x65*/&&
        ERR_GET_FUNC(err_code) != 104 /*DSO_F_DL_BIND_FUNC=0x68*/
        )
    ) return;
    (void)ERR_get_error();

    /* 2506C06A from DSO method */
    err_code = ERR_peek_error();
    if (ERR_GET_LIB(err_code) != ERR_LIB_DSO /*37=0x25*/&&
        ERR_GET_REASON(err_code) != DSO_R_SYM_FAILURE &&
        ERR_GET_FUNC(err_code) != 108 /*DSO_F_DSO_BIND_FUNC=0x6C*/
    ) return;
    (void)ERR_get_error();
}
#else
static inline void
clear_err_dso_bind_func(void) {}
#endif


#if 0
#  define USE_OPENSSL_VERIFY_PROMPT
#endif
static char*
nss_pass_func_ui_method(PK11SlotInfo *slot, const UI_METHOD *ui_method, void *ui_data) {
    char *ret = NULL;
    UI *ui = NULL;

    CALL_TRACE("PK11_GetSlotName()='%s'\n", PK11_GetSlotName (slot));
    CALL_TRACE("PK11_GetTokenName()='%s'\n", PK11_GetTokenName(slot));
    CALL_TRACE("PK11_GetMinimumPwdLength()=%d\n", PK11_GetMinimumPwdLength(slot));
    CALL_TRACE("NSS_UI ui_method=%p, ui_data=%p\n", (void*)ui_method, ui_data);

    if (ui_method == NULL) goto done;

    ui = UI_new_method(ui_method);
    if (ui == NULL) goto done;

    if (ui_data)
        UI_add_user_data(ui, ui_data);

    /*
     * Before to ask for password output last error as it may contain
     * important information.
     * Remark: try to clear non-significant errors left in queue.
     */
    clear_err_dso_bind_func();
    UI_ctrl(ui, UI_CTRL_PRINT_ERRORS, 1, 0, 0);

    { /*promt for password */
        int   ok;
        char *prompt;

        int   ui_flags = UI_INPUT_FLAG_DEFAULT_PWD;
        int   min_len = PK11_GetMinimumPwdLength(slot);
        int   max_len = 100;
        char *buf1;
    #ifdef USE_OPENSSL_VERIFY_PROMPT
        char *buf2;
    #endif

        buf1 = OPENSSL_malloc(max_len);
    #ifdef USE_OPENSSL_VERIFY_PROMPT
        buf2 = OPENSSL_malloc(max_len);
    #endif

        prompt = UI_construct_prompt(ui, "pass phrase", PK11_GetTokenName(slot));
        if (prompt == NULL) goto passdone;

        CALL_TRACE("prompt='%s'\n", prompt);

    #if 0 /* not yet */
        UI_add_info_string(ui, PK11_GetSlotName (slot));
    #endif

        ok = UI_add_input_string(ui, prompt, ui_flags, buf1, min_len, max_len - 1);
        if (ok < 0) goto passdone;

    #ifdef USE_OPENSSL_VERIFY_PROMPT
        ok = UI_add_verify_string(ui, prompt, ui_flags, buf2, min_len, max_len - 1, buf1);
        if (ok < 0) goto passdone;
    #endif

        do {
            ok = UI_process(ui);
        } while (ok < 0 && UI_ctrl(ui, UI_CTRL_IS_REDOABLE, 0, 0, 0));

        ret = PL_strdup(buf1);

passdone:
        CALL_TRACE("buf1='%s'\n", buf1);
    #ifdef USE_OPENSSL_VERIFY_PROMPT
        CALL_TRACE("buf2='%s'\n", buf2);
    #endif
        CALL_TRACE("UI result[0]='%s'\n", UI_get0_result(ui, 0));
    #ifdef USE_OPENSSL_VERIFY_PROMPT
        CALL_TRACE("UI result[1]='%s'\n", UI_get0_result(ui, 1));
    #endif
        /* DO NOT READ MORE as ui_lib.c raise error !
        CALL_TRACE(ctx, "UI result[2]='%s'\n", UI_get0_result(ui, 2)); */

        if (buf1) {
            OPENSSL_cleanse(buf1, max_len);
            OPENSSL_free(buf1);
        }
    #ifdef USE_OPENSSL_VERIFY_PROMPT
        if (buf2) {
            OPENSSL_cleanse(buf2, max_len);
            OPENSSL_free(buf2);
        }
    #endif
        if (prompt)
            OPENSSL_free(prompt);
    }

done:
    if (ui)
        UI_free(ui);
    return ret;
}
#ifdef USE_OPENSSL_VERIFY_PROMPT
# undef USE_OPENSSL_VERIFY_PROMPT
#endif


static char*
nss_pass_func(PK11SlotInfo *slot, PRBool retry, void *arg) {
    CALL_TRACE("args: slot=%p, retry=%d, arg=%p\n", (void*)slot, retry, (void*)arg);

    if (arg == NULL) return NULL;
    if (retry) return NULL;

{   NSS_UI *wincx = (NSS_UI*)arg;
    return nss_pass_func_ui_method(slot, wincx->ui_method, wincx->ui_data);
}
}
