/**
 * NSS Engine
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2011-2023 Roumen Petrov
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#ifndef USE_OPENSSL_PROVIDER
/* TODO: implement OpenSSL 4.0 API, as OpenSSL 3.* is quite nonfunctional */
# define OPENSSL_SUPPRESS_DEPRECATED
#endif

#define OPENSSL_OCSP_H /* see e_nss_store.c */
#include <openssl/engine.h>
#include <openssl/pem.h>

#include "e_nss_int.h"
#include "e_nss_err.c"

#ifndef ENGINE_CMD_BASE
#  include "ENGINE_CMD_BASE is not defined"
#endif

#include <prinit.h>
#include <nss.h>
#if 1
#  define NSS_VER_CHECK	((((NSS_VMAJOR<<8)|NSS_VMINOR)<<8)|NSS_VPATCH)
#  if NSS_VER_CHECK < 0x030903
#    include "NSS version < 3.9.3 is not supported"
#  endif
#endif
#include <cert.h>
#include <keyhi.h>
#include <cryptohi.h>
#include <secerr.h>


/*
 * Constants used when creating the ENGINE
 */

const char *nss_engine_id = "e_nss";
/* NSS_ENG_VERSION is defined in $(top_srcdir)/version.m4 */
static const char *nss_engine_name = "NSS engine (" NSS_ENG_VERSION ")";


/*
 * Logging functions
 */

#ifdef ENABLE_CALL_TRACE
void
trace_calls(const char *func, const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "TRACE_E_NSS:%s()  ", func);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
}
#endif


static void
nss_vtrace(NSS_CTX *ctx, int level, const char *fmt, va_list ap) {
    BIO *err;

    if (ctx == NULL) return;
    if (ctx->debug_level < level) return;

    if (ctx->error_file)
        err = BIO_new_file(ctx->error_file, "a+");
    else
        err = BIO_new_fp(stderr, BIO_NOCLOSE);

    if (!err) return;

    switch (level) {
    case NSS_LOGLEVEL_VERBOSE: BIO_puts(err, "INFO["); break;
    case NSS_LOGLEVEL_DEBUG  : BIO_puts(err, "DEBUG["  ); break;
    case NSS_LOGLEVEL_TRACE  : BIO_puts(err, "TRACE["  ); break;
    }
    BIO_puts(err, nss_engine_id);
    BIO_puts(err, "] ");

    BIO_vprintf(err, fmt, ap);
    BIO_free(err);
}

static void
nss_verbose(NSS_CTX *ctx, const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    nss_vtrace(ctx, NSS_LOGLEVEL_VERBOSE, fmt, ap);
    va_end(ap);
}

static void
nss_debug(NSS_CTX *ctx, const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    nss_vtrace(ctx, NSS_LOGLEVEL_DEBUG, fmt, ap);
    va_end(ap);
}

#ifdef ENABLE_NSS_TRACE
void
f_nss_trace(const char *func, NSS_CTX *ctx, const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
{   char fmtbuf[4096];
    if (snprintf(fmtbuf, sizeof(fmtbuf), "%s(): %s", func, fmt) < 0)
      nss_vtrace(ctx, NSS_LOGLEVEL_TRACE, fmt, ap);
    else
      nss_vtrace(ctx, NSS_LOGLEVEL_TRACE, fmtbuf, ap);
}
    va_end(ap);
}
#endif


/*
 * Context management
 */

NSS_CTX conf_ctx = { NULL, 0, NULL };

static inline NSS_CTX*
NSS_CTX_new(void) {
    NSS_CTX* ctx = OPENSSL_malloc(sizeof(NSS_CTX));

    /* NOTE: during initialization we cannot set OpenSSL errors! */
    if (ctx == NULL) return NULL;

    if (conf_ctx.config_dir)
        ctx->config_dir = NSS_strdup(conf_ctx.config_dir);
    else
        ctx->config_dir = NULL;

    ctx->debug_level = conf_ctx.debug_level;

    if (conf_ctx.error_file)
        ctx->error_file = NSS_strdup(conf_ctx.error_file);
    else
        ctx->error_file = NULL;

    return ctx;
}

static inline void
NSS_CTX_free(NSS_CTX *ctx) {
    if (ctx == NULL) return;

    OPENSSL_free((void*)ctx->config_dir);
    OPENSSL_free((void*)ctx->error_file);
    OPENSSL_free(ctx);
}


NSS_KEYCTX*
NSS_KEYCTX_new(void) {
    NSS_KEYCTX *keyctx = OPENSSL_malloc(sizeof(NSS_KEYCTX));

    if (keyctx)
        memset(keyctx, 0, sizeof(NSS_KEYCTX));
    return keyctx;
}


void
NSS_KEYCTX_free(NSS_KEYCTX *keyctx) {
    if (keyctx == NULL) return;

    if (keyctx->pvtkey) {
        SECKEY_DestroyPrivateKey(keyctx->pvtkey);
        keyctx->pvtkey = NULL;
    }
    if (keyctx->pubkey) {
        SECKEY_DestroyPublicKey(keyctx->pubkey);
        keyctx->pubkey = NULL;
    }
    if (keyctx->cert) {
        CERT_DestroyCertificate(keyctx->cert);
        keyctx->cert = NULL;
    }
    OPENSSL_free(keyctx);
}


#if 0
static int
CRYPTO_EX_NSS_CTX_new(void *parent, void *ptr, CRYPTO_EX_DATA *ad, int idx, long argl, void *argp) {
    CALL_TRACE("args: parent=%p, ptr=%p, ad=%p, idx=%d, argl=%ld, argp=%p\n",
      parent   , ptr   , ad   , idx   , argl    , argp);

    if (argp)
        CALL_TRACE("CRYPTO_EX_NSS_CTX_new()  argp=%s\n", argp);
    return 0;
}
static int
CRYPTO_EX_NSS_CTX_dup(CRYPTO_EX_DATA *to, CRYPTO_EX_DATA *from, void *from_d, int idx, long argl, void *argp) {
    CALL_TRACE("args: to=%p, from=%p, from_d=%p, idx=%d, argl=%ld, argp=%p\n",
      to   , from   , from_d   , idx   , argl    , argp);

    if (argp)
        CALL_TRACE("CRYPTO_EX_NSS_CTX_dup()  argp=%s\n", argp);
    return 0;
}
static void
CRYPTO_EX_NSS_CTX_free(void *parent, void *ptr, CRYPTO_EX_DATA *ad, int idx, long argl, void *argp) {
    CALL_TRACE("args: parent=%p, ptr=%p, ad=%p, idx=%d, argl=%ld, argp=%p\n",
      parent   , ptr   , ad   , idx   , argl    , argp);

    if (argp)
        CALL_TRACE("argp=%s\n", argp);

    {
        ENGINE *e = (ENGINE *)(parent);
        CALL_TRACE("id=%s\n", ENGINE_get_id(e));
        CALL_TRACE("name=%s\n", ENGINE_get_name(e));
    }
}
#endif


static int/*bool*/
nss_openssl_fips_mode(void) {
#if defined(HAVE_EVP_DEFAULT_PROPERTIES_IS_FIPS_ENABLED)    /* OpenSSL >= 3.0 */
    return EVP_default_properties_is_fips_enabled(NULL);
#elif defined(HAVE_FIPS_MODE)
    return FIPS_mode();
#else
    return 0;
#endif
}


int/*bool*/
nss_init_database(
    const char *file, int line, const char *funcname,
    int function, NSS_CTX *ctx
) {
    if (!NSS_IsInitialized()) {
        if (ctx->config_dir == NULL) {
            ERR_NSS_error(file, line, funcname, function,
                NSS_R_DB_IS_NOT_INITIALIZED);
            return 0;
        }
        nss_trace(ctx, "NSS_Init('%s') ...\n", ctx->config_dir);
        if (NSS_Init(ctx->config_dir) != SECSuccess) {
            ERR_NSS_error(file, line, funcname, function,
                NSS_R_CANNOT_SETUP_CONFIG_DIR);
            return 0;
        }
        nss_trace(ctx, "FIPS mode: %d, PK11_IsFIPS()=%d\n",
            nss_openssl_fips_mode(), PK11_IsFIPS()
        );
        if (nss_openssl_fips_mode()) {
            if (!PK11_IsFIPS()) {
                ERR_NSS_error(file, line, funcname, function,
                    NSS_R_NOT_IN_FIPS_MODE);
                return 0;
            }
        }
    }
    if (!NSS_IsInitialized()) {
        ERR_NSS_error(file, line, funcname, function,
            NSS_R_DB_IS_NOT_INITIALIZED);
        return 0;
    }
    return 1;
}


/*
 * Constants used when creating the context extra data
 */
static int nss_eng_ctx_index = -1;

NSS_CTX*
ENGINE_get_NSS_CTX(const ENGINE *e) {
    return ENGINE_get_ex_data(e, nss_eng_ctx_index);
}

static int/*bool*/
ENGINE_set_NSS_CTX(ENGINE *e, NSS_CTX* ctx) {
    return ENGINE_set_ex_data(e, nss_eng_ctx_index, ctx);
}


/* ================================================================= */

#include "e_nss_cmd.c"
#include "e_nss_ui.c"
#include "e_nss_key.c"


/*
 * Functions to handle the engine
 */

static int/*bool*/
nss_init_eng_ctx(ENGINE *e) {
    CALL_TRACE("e=%p, nss_eng_ctx_index=%d\n", (void*)e, nss_eng_ctx_index);

    if (nss_eng_ctx_index < 0) {
        /*NOTE crash if nss_init_eng_ctx() called from bind_nss*/
    #if 1
        nss_eng_ctx_index = ENGINE_get_ex_new_index(0, NULL, NULL, NULL, NULL);
    #else
        /*NOTE crash if CRYPTO_EX_NSS_CTX_free is set*/
        nss_eng_ctx_index = ENGINE_get_ex_new_index(0, "Test by Roumen", CRYPTO_EX_NSS_CTX_new, CRYPTO_EX_NSS_CTX_dup, CRYPTO_EX_NSS_CTX_free);
    #endif
    }

    CALL_TRACE("nss_eng_ctx_index=%d\n", nss_eng_ctx_index);
    if (nss_eng_ctx_index < 0) return 0;

    if (ENGINE_get_NSS_CTX(e) == NULL) {
        NSS_CTX *ctx = NSS_CTX_new();

        CALL_TRACE("ctx=%p\n", (void*)ctx);
        if (ctx == NULL) return 0;

        ENGINE_set_NSS_CTX(e, ctx);
    }
    return 1;
}


static int/*bool*/
nss_init(ENGINE *e) {
    int ret = 0;

    CALL_TRACE("args: e=%p\n", (void*)e);

    /* ensure engine context index */
    if (!nss_init_eng_ctx(e)) {
        NSSerr(NSS_F_INIT, NSS_R_ENG_CTX_INDEX);
        goto done;
    }

    /* ensure that nss globals are set up */
#if 0
    /* NOTE: NSPR is now implicitly initialized, usually by the first
     * NSPR function called by a program.
     */
    PR_Init(PR_USER_THREAD, PR_PRIORITY_NORMAL, 0);
#endif
    PK11_SetPasswordFunc(nss_pass_func);
/*
 * Not yet (see comment in pk11wrap/pk11auth.c): "... we simply tell the
 * server code that it should now verify the clients password and tell us
 * the results."
    PK11_SetVerifyPasswordFunc(...);
*/

    ret = 1;
done:
    CALL_TRACE("^ %s\n", (ret ? "ok": ""));
    return ret;
}


static int
nss_finish(ENGINE *e) {
    CALL_TRACE("...\n");

    if (NSS_IsInitialized()) {
        SECStatus  rv;

        CALL_TRACE("NSS_Shutdown() ...\n");
        rv = NSS_Shutdown();
        if (rv != SECSuccess) {
            NSSerr(NSS_F_FINISH, NSS_R_SHUTDOWN_FAIL);
        }
    }

    if (nss_eng_ctx_index >= 0) {
        NSS_CTX *ctx;

        ctx = ENGINE_get_NSS_CTX(e);
        ENGINE_set_NSS_CTX(e, NULL);
/* there is no need to get index again on next initialization
        nss_eng_ctx_index = -1;
*/
        NSS_CTX_free(ctx);
    }

    return 1;
}


static int
nss_destroy(ENGINE *e) {
    UNUSED(e);
    CALL_TRACE("...\n");

#ifdef E_NSS_STORE_LOADER
    destroy_nss_store(e);
#endif

#ifdef E_NSS_RSA_METHOD
    destroy_nss_rsa_method(e);
#endif

#ifdef E_NSS_DSA_METHOD
    destroy_nss_dsa_method(e);
#endif

#ifdef E_NSS_ECDSA_METHOD
    destroy_nss_ecdsa_method(e);
#endif

#ifdef E_NSS_EC_METHOD
    destroy_nss_ec_method(e);
#endif

    if (conf_ctx.config_dir != NULL) {
        OPENSSL_free((void*)conf_ctx.config_dir);
        conf_ctx.config_dir= NULL;
    }
    if (conf_ctx.error_file != NULL) {
        OPENSSL_free((void*)conf_ctx.error_file);
        conf_ctx.error_file= NULL;
    }

    ERR_unload_NSS_strings();

    (void)PR_Cleanup();

    return 1;
}


#ifdef ENGINE_METHOD_PKEY_METHS
/* For OpenSSL 1.0.* compatibility (some initial versions):
 * - sign/verify fail if engine does not define pkey_meths
 */
#  define USE_NSS_PKEY_METHS_STUB
/* pkey_meths may require digest */
#  define USE_NSS_DIGEST_STUB
#endif


#ifdef USE_NSS_DIGEST_STUB
static int
nss_digest_nids[] = { 0 };

static int
nss_digests(ENGINE *e, const EVP_MD **digest, const int **nids, int nid) {
    UNUSED(e);
    if (!digest) {
        CALL_TRACE("requested nid list\n");
        *nids = nss_digest_nids;
        return 0;
    }

    CALL_TRACE("requested nid=%d\n", nid);
    *digest = EVP_get_digestbynid(nid);
    return *digest ? 1 : 0;
}
#endif /*def USE_NSS_DIGEST_STUB*/


#ifdef USE_NSS_PKEY_METHS_STUB
static int
nss_pkey_meth_nids[] = { 0 };

static int
nss_pkey_meths(ENGINE *e, EVP_PKEY_METHOD **pmeth, const int **nids, int nid) {
    UNUSED(e);
    if (!pmeth) {
        CALL_TRACE("requested nid list\n");
        *nids = nss_pkey_meth_nids;
        return 0;
    }

    CALL_TRACE("requested nid=%d\n", nid);
    *pmeth = (EVP_PKEY_METHOD*)EVP_PKEY_meth_find(nid);
    return *pmeth ? 1 : 0;
}
#endif /*def USE_NSS_PKEY_METHS_STUB*/


static int/*bool*/
bind_nss(ENGINE *e) {
    CALL_TRACE("args: e=%p\n", (void*)e);

    if (!ENGINE_set_id(e, nss_engine_id)
    ||  !ENGINE_set_name(e, nss_engine_name)
#ifdef ENGINE_FLAGS_NO_REGISTER_ALL
/* From openssl(1.0.1)/crypto/engine/engine.h:
 * This flag if for an ENGINE that does not want its methods registered as
 * part of ENGINE_register_all_complete() for example if the methods are
 * not usable as default methods.
 *
 * NOTE
 * - we would like openssl internal engines to be initialized first
 * - flag does not override default_algorithms = ALL set in engine
 *   configuration file
 */
    ||  !ENGINE_set_flags(e, ENGINE_FLAGS_NO_REGISTER_ALL)
#endif
    ||  !ENGINE_set_cmd_defns(e, nss_cmd_defns)
    ||  !ENGINE_set_ctrl_function(e, nss_ctrl)
    ||  !ENGINE_set_init_function(e, nss_init)
    ||  !ENGINE_set_finish_function(e, nss_finish)
    ||  !ENGINE_set_destroy_function(e, nss_destroy)
    ||  !ENGINE_set_load_privkey_function(e, nss_load_privkey)
    ||  !ENGINE_set_load_pubkey_function(e, nss_load_pubkey)
  /*||  !ENGINE_set_ciphers(e, nss_ciphers)*/
#ifdef USE_NSS_DIGEST_STUB
    ||  !ENGINE_set_digests(e, nss_digests)
#else
  /*||  !ENGINE_set_digests(e, nss_digests)*/
#endif
#ifdef USE_NSS_PKEY_METHS_STUB
    ||  !ENGINE_set_pkey_meths(e, nss_pkey_meths)
  /*||  !ENGINE_register_pkey_meths(e) ???*/
#endif
    )
        return 0;

#ifdef E_NSS_STORE_LOADER
    if (!bind_nss_store(e))
        return 0;
#endif

#ifdef E_NSS_RSA_METHOD
    if (!bind_nss_rsa_method(e))
        return 0;
#endif

#ifdef E_NSS_DSA_METHOD
    if (!bind_nss_dsa_method(e))
        return 0;
#endif

#ifdef E_NSS_EC_METHOD
    if (!bind_nss_ec_method(e))
        return 0;
#endif

#ifdef E_NSS_ECDSA_METHOD
    if (!bind_nss_ecdsa_method(e))
        return 0;
#endif

#if 0
CRASH !?!?
    if (!nss_init_eng_ctx(e))
        return 0;
#endif

    /* ensure the nss error handling is set up */
    ERR_load_NSS_strings();

    return 1;
}


#if defined(OPENSSL_VERSION_NUMBER) && (OPENSSL_VERSION_NUMBER < 0x00908000L)
/* NOTE set dynamic/static support defines in 0.9.8 style */
#  undef OPENSSL_NO_DYNAMIC_ENGINE
#  undef OPENSSL_NO_STATIC_ENGINE

#  ifdef PIC
#    ifndef ENGINE_DYNAMIC_SUPPORT
#      define ENGINE_DYNAMIC_SUPPORT
#    endif
#  endif

#  ifdef ENGINE_DYNAMIC_SUPPORT
#    define OPENSSL_NO_STATIC_ENGINE
#  else
#    define OPENSSL_NO_DYNAMIC_ENGINE
#  endif

#endif


/* TODO libtool and openssl integration - has to be in configure */
#ifdef PIC
#  ifdef OPENSSL_NO_DYNAMIC_ENGINE
#    include "build requires dynamic engine"
#  endif
#else
#  ifdef OPENSSL_NO_STATIC_ENGINE
#    include "build requires static engine"
#  endif
#endif


#ifndef OPENSSL_NO_DYNAMIC_ENGINE
static int/*bool*/
bind_helper(ENGINE *e, const char *id) {

    CALL_TRACE("args: e=%p, id=%s\n", (void*)e, (id ? id: "<none>"));

    if (id && (strcmp(id, nss_engine_id) != 0))
        return 0;
    return bind_nss(e);
}

#ifdef HAVE_EC_KEY_GET0_ENGINE	/* OpenSSL >= 1.1.1 */
#if defined(OPENSSL_VERSION_NUMBER) && OPENSSL_VERSION_NUMBER < 0x10200000L
/* Avoid post OpenSSL 1.1.1m indirect changes in application
   functionality - bind function will stop "at exit". */
int
OPENSSL_init_crypto(uint64_t opts, const OPENSSL_INIT_SETTINGS *settings) {
    UNUSED(opts);
    UNUSED(settings);
    return 0;
}
#endif
#endif /*def HAVE_EC_KEY_GET0_ENGINE*/

IMPLEMENT_DYNAMIC_CHECK_FN()
IMPLEMENT_DYNAMIC_BIND_FN(bind_helper)
#endif /*ndef OPENSSL_NO_DYNAMIC_ENGINE*/


#ifndef OPENSSL_NO_STATIC_ENGINE
static ENGINE *
engine_nss(void) {
    ENGINE *e;

    CALL_TRACE("...\n");

    e = ENGINE_new();
    if (!e) return NULL;

    if (!bind_nss(e)) {
        ENGINE_free(e);
        return NULL;
    }
    return e;
}


extern void ENGINE_load_nss(void);
void
ENGINE_load_nss(void) {
    /* copied from eng_[openssl|dyn].c */
    ENGINE *e;

    CALL_TRACE("...\n");

    e = engine_nss();
    if (!e) return;
    ENGINE_add(e);
    ENGINE_free(e);
    ERR_clear_error();
}
#endif /*ndef OPENSSL_NO_STATIC_ENGINE*/
