/**
 * NSS Engine - EVP keys
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2017-2023 Roumen Petrov
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#ifndef USE_OPENSSL_PROVIDER
/* TODO: implement OpenSSL 4.0 API, as OpenSSL 3.* is quite nonfunctional */
# define OPENSSL_SUPPRESS_DEPRECATED
#endif

#ifdef E_NSS_STORE_LOADER
/* Notes:
 * Since OpenSSL 0.9.8 engine header includes x509 header.
 * In OpenSSL 3+ x509 header is polluted by ocsp:
 * .../nss/certt.h:562:0: warning: "KU_DIGITAL_SIGNATURE" redefined
 * #define KU_DIGITAL_SIGNATURE (0x80) ....
 * ... x509v3.h:662:0: note: this is the location of the previous definition
 * # define KU_DIGITAL_SIGNATURE    0x0080
 * As work-around we mimic inclusion of ocsp header.
 */
#define OPENSSL_OCSP_H
#include <openssl/x509.h>
#include <openssl/engine.h>
#include <openssl/store.h>

#include "e_nss_int.h"
#include "e_nss_err.h"

#include <cert.h>

extern const char *nss_engine_id;

extern X509* X509_from_CERTCertificate(const CERTCertificate *cert);


/* ===== NSS STORE loader ===== */

static const char nss_scheme[] = "nss";

enum NSS_QUERY_TYPE {
    NSS_QUERY_NONE,
    NSS_QUERY_KEY,
    NSS_QUERY_PUBKEY,
    NSS_QUERY_CERT,
    NSS_QUERY_NAME,
    NSS_QUERY_LISTALL,
    NSS_QUERY_LISTUSER,
    NSS_QUERY_LISTCA
};

struct ossl_store_loader_ctx_st {
    enum NSS_QUERY_TYPE type;

    int query_phase;
    int error; /*bool*/

    const char *nickname;

    CERTCertList     *list;
    CERTCertListNode *node;
};


static OSSL_STORE_LOADER_CTX*
OSSL_STORE_LOADER_CTX_new(void) {
    OSSL_STORE_LOADER_CTX *ctx;

    ctx = OPENSSL_malloc(sizeof(OSSL_STORE_LOADER_CTX));
    if (ctx == NULL) return NULL;

    ctx->type = NSS_QUERY_NONE;

    ctx->query_phase = 0;
    ctx->error = 0;

    ctx->nickname = NULL;

    ctx->list = NULL;
    ctx->node = NULL;

    return ctx;
}


static void
OSSL_STORE_LOADER_CTX_free(OSSL_STORE_LOADER_CTX* ctx) {
    if (ctx == NULL) return;

    OPENSSL_free((void*)ctx->nickname);
    OPENSSL_free(ctx);
}


static OSSL_STORE_LOADER_CTX*
nss_store_open(
    const OSSL_STORE_LOADER *loader, const char *uri,
    const UI_METHOD *ui_method, void *ui_data
) {

{   size_t len;

    CALL_TRACE("args: uri='%s'\n", uri);

    len = strlen(nss_scheme);
    if (strncmp(uri, nss_scheme, len) != 0) return NULL;

    uri += len;
    if (*uri++ != ':') return NULL;
    if (*uri == '\0') return NULL;
}
    CALL_TRACE("query='%s'\n", uri);

{   const ENGINE *e;
    NSS_CTX *e_ctx;

    e = OSSL_STORE_LOADER_get0_engine(loader);
    if (e == NULL) return NULL;

    e_ctx = ENGINE_get_NSS_CTX(e);
    if (e_ctx == NULL) return NULL;

    if (!NSS_INIT_DATABASE(NSS_F_STORE_OPEN, e_ctx))
        return NULL;
}

{   OSSL_STORE_LOADER_CTX *ctx;
    NSS_UI wincx = { ui_method, ui_data };

    ctx = OSSL_STORE_LOADER_CTX_new();
    if (ctx == NULL) return NULL;

    if (strncmp(uri, "list=all", 8) == 0) {
        ctx->type = NSS_QUERY_LISTALL;
        uri += 8;
        if (*uri == '\0')
            ctx->list = PK11_ListCerts(PK11CertListAll, &wincx);
    } else if (strncmp(uri, "list=ca", 7) == 0) {
        ctx->type = NSS_QUERY_LISTCA;
        uri += 7;
        if (*uri == '\0')
            ctx->list = PK11_ListCerts(PK11CertListCA, &wincx);
    } else if (strncmp(uri, "list=user", 9) == 0) {
        ctx->type = NSS_QUERY_LISTUSER;
        uri += 9;
        if (*uri == '\0')
            ctx->list = PK11_ListCerts(PK11CertListUser, &wincx);
    } else if (strncmp(uri, "cert=", 5) == 0) {
        ctx->type = NSS_QUERY_CERT;
        uri += 5;
        if (strlen(uri) > 0)
            ctx->nickname = NSS_strdup(uri);
    } else if (strncmp(uri, "key=", 4) == 0) {
        ctx->type = NSS_QUERY_KEY;
        uri += 4;
        if (strlen(uri) > 0)
            ctx->nickname = NSS_strdup(uri);
    } else {
        ctx->type = NSS_QUERY_NAME;
        if (strlen(uri) > 0)
            ctx->nickname = NSS_strdup(uri);
    }

    switch(ctx->type) {
    case NSS_QUERY_LISTALL:
    case NSS_QUERY_LISTUSER:
    case NSS_QUERY_LISTCA: {
        if (ctx->list == NULL) {
            ctx->error = 1;
            break;
        }
        ctx->node = CERT_LIST_HEAD(ctx->list);
        } break;
    case NSS_QUERY_KEY:
    case NSS_QUERY_CERT:
    case NSS_QUERY_NAME: {
        if (ctx->nickname == NULL) {
            ctx->query_phase = -1;
            ctx->error = 1;
        }
        } break;
    default:
        break;;
    };

    CALL_TRACE("return %p\n", (void*)ctx);
    return ctx;
}
}


static int
nss_store_expect(OSSL_STORE_LOADER_CTX *ctx, int expected) {
    CALL_TRACE("expected: %d\n", expected);
    switch (expected) {
    case OSSL_STORE_INFO_CERT:
        ctx->type = NSS_QUERY_CERT;
        break;
    case OSSL_STORE_INFO_PKEY:
        ctx->type = NSS_QUERY_KEY;
        break;
#ifdef OSSL_STORE_INFO_PUBKEY	/* OpenSSL >= 3.0 */
    case OSSL_STORE_INFO_PUBKEY:
        ctx->type = NSS_QUERY_PUBKEY;
        break;
#endif
    default:
        return 0;
    }

    CALL_TRACE("ctx->type: %d\n", ctx->type);
    return 1;
}


static int
nss_store_ctrl(OSSL_STORE_LOADER_CTX *ctx, int cmd, va_list args) {
    CALL_TRACE("args: ctx=%p, cmd=%d\n", (void*)ctx, cmd);
    UNUSED(args);
    return 0;
}


static OSSL_STORE_INFO*
nss_store_load_pkey(OSSL_STORE_LOADER_CTX *ctx, const UI_METHOD *ui_method, void *ui_data) {
    EVP_PKEY* pkey;

{   ENGINE *e = ENGINE_by_id(nss_engine_id);
    if (ctx->type == NSS_QUERY_PUBKEY)
        pkey = nss_load_pubkey_c(e, ctx->nickname, ui_method, ui_data);
    else
        pkey = nss_load_privkey_c(e, ctx->nickname, ui_method, ui_data);
    ENGINE_free(e);

    if (pkey == NULL) return NULL;
}

#ifdef OSSL_STORE_INFO_PUBKEY		/*OpenSSL >= 3.0*/
    if (ctx->type == NSS_QUERY_PUBKEY)
        return OSSL_STORE_INFO_new_PUBKEY(pkey);
#endif

    return OSSL_STORE_INFO_new_PKEY(pkey);
}


static OSSL_STORE_INFO*
nss_store_load_cert(OSSL_STORE_LOADER_CTX *ctx, const UI_METHOD *ui_method, void *ui_data) {
    X509 *x;

{   CERTCertificate  *cert;
    NSS_UI wincx = { ui_method, ui_data };

    cert = PK11_FindCertFromNickname(ctx->nickname, &wincx);
    if (cert == NULL) return NULL;

    x = X509_from_CERTCertificate(cert);
    if (x == NULL) return NULL;
}

    return OSSL_STORE_INFO_new_CERT(x);
}


static OSSL_STORE_INFO*
nss_store_load(OSSL_STORE_LOADER_CTX *ctx, const UI_METHOD *ui_method, void *ui_data) {
    OSSL_STORE_INFO *ret = NULL;

    CALL_TRACE("args: ctx=%p, ui_method=%p, ui_data=%p\n",
        (void*)ctx, (void*)ui_method, (void*)ui_data);

    switch(ctx->type) {
    case NSS_QUERY_LISTALL:
    case NSS_QUERY_LISTUSER:
    case NSS_QUERY_LISTCA: {
        if (ctx->list == NULL) {
            ctx->error = 1;
            break;
        }
        if (!CERT_LIST_END(ctx->node, ctx->list)) {
            CERTCertificate *cert = ctx->node->cert;
            char *uri;
            size_t len;

            len = strlen(cert->nickname) + 1;
            uri = OPENSSL_malloc(len + 5);
            memmove(uri, "cert=", 5);
            memmove(uri + 5, cert->nickname, len);
            ret = OSSL_STORE_INFO_new_NAME(uri);

            ctx->node = CERT_LIST_NEXT(ctx->node);
        }
        } break;
    case NSS_QUERY_PUBKEY:
    case NSS_QUERY_KEY: {
        if (ctx->query_phase == 0) {
            ret = nss_store_load_pkey(ctx, ui_method, ui_data);
        }
        ctx->query_phase = -1;
        } break;
    case NSS_QUERY_CERT: {
        if (ctx->query_phase == 0) {
            ret = nss_store_load_cert(ctx, ui_method, ui_data);
        }
        ctx->query_phase = -1;
        } break;
    case NSS_QUERY_NAME: {
        switch (ctx->query_phase) {
        case 0: {
            ret = nss_store_load_pkey(ctx, ui_method, ui_data);
            if (ret != NULL)
                ctx->query_phase++;
            else
                ctx->query_phase = -1;
            } break;
        case 1: {
            ret = nss_store_load_cert(ctx, ui_method, ui_data);
            ctx->query_phase = -1;
            }  break;
        default: {
            ctx->query_phase = -1;
            }  break;
        }
        } break;
    default:
        break;
    };

    CALL_TRACE("return %p\n", (void*)ret);
    return ret;
}


static int
nss_store_eof(OSSL_STORE_LOADER_CTX *ctx) {
    int ret = 1;

    CALL_TRACE("args: ctx=%p\n", (void*)ctx);

    switch(ctx->type) {
    case NSS_QUERY_LISTALL:
    case NSS_QUERY_LISTUSER:
    case NSS_QUERY_LISTCA: {
        if (ctx->list != NULL)
            ret = CERT_LIST_END(ctx->node, ctx->list) ? 1 : 0;
        } break;
    case NSS_QUERY_KEY:
    case NSS_QUERY_PUBKEY:
    case NSS_QUERY_CERT:
    case NSS_QUERY_NAME: {
        ret = ctx->query_phase < 0;
        } break;
    default:
        break;
    }

    CALL_TRACE("return %d\n", ret);
    return ret;
}


static int
nss_store_error(OSSL_STORE_LOADER_CTX *ctx) {
    int ret = ctx->error;

    CALL_TRACE("return %d\n", ret);
    return ret;
}


static int
nss_store_close(OSSL_STORE_LOADER_CTX *ctx) {
    CALL_TRACE("args: ctx=%p\n", (void*)ctx);
    switch(ctx->type) {
    case NSS_QUERY_LISTALL:
    case NSS_QUERY_LISTUSER:
    case NSS_QUERY_LISTCA: {
        if (ctx->list == NULL)
            break;
        CERT_DestroyCertList(ctx->list);
        ctx->list = NULL;
        } break;
    default:
        break;
    }

    OSSL_STORE_LOADER_CTX_free(ctx);
    return 1;
}


int/*bool*/
bind_nss_store(ENGINE *e) {
    OSSL_STORE_LOADER *loader = OSSL_STORE_LOADER_new(e, nss_scheme);

    if (loader == NULL) return 0;

    if (!OSSL_STORE_LOADER_set_open(loader, nss_store_open)
    ||  !OSSL_STORE_LOADER_set_expect(loader, nss_store_expect)
    ||  !OSSL_STORE_LOADER_set_ctrl(loader, nss_store_ctrl)
    ||  !OSSL_STORE_LOADER_set_load(loader, nss_store_load)
    ||  !OSSL_STORE_LOADER_set_eof(loader, nss_store_eof)
    ||  !OSSL_STORE_LOADER_set_error(loader, nss_store_error)
    ||  !OSSL_STORE_LOADER_set_close(loader, nss_store_close)
    ||  !OSSL_STORE_register_loader(loader)
    )
        goto err;

    return 1;
err:
    OSSL_STORE_LOADER_free(loader);
    return 0;
}

void
destroy_nss_store(ENGINE *e) {
    OSSL_STORE_LOADER *loader;

CALL_TRACE("e=%p, nss_scheme='%s'\n", (void*)e, nss_scheme);
    /*TODO crash if engine perform sign/verify operation*/
    loader = OSSL_STORE_unregister_loader(nss_scheme);
CALL_TRACE("loader=%p\n", (void*)loader);

    OSSL_STORE_LOADER_free(loader);
}


int/*bool*/
nss_cmd_list_cert_store(BIO *out, long i) {
    OSSL_STORE_CTX *store_ctx;

{   char uri[128];
    const char *p;
    switch (i) {
    case 1: p = "user"; break;
    case 2: p = "ca"  ; break;
    case 3: p = "all" ; break;
    default: return 0;
    }
    (void)snprintf(uri, sizeof(uri), "%s:list=%s", nss_scheme, p);

    store_ctx = OSSL_STORE_open(uri, NULL, NULL, NULL, NULL);
}

    while (!OSSL_STORE_eof(store_ctx) ) {
        OSSL_STORE_INFO *store_info;

        store_info = OSSL_STORE_load(store_ctx);
        if (store_info == NULL) break;

{       int info_type = OSSL_STORE_INFO_get_type(store_info);
        if (info_type != OSSL_STORE_INFO_NAME) {
            OSSL_STORE_INFO_free(store_info);
            continue;
        }
}
{       const char *info_name = OSSL_STORE_INFO_get0_NAME(store_info);
        /* NOTE info result from list queries is in format cert=... */
        BIO_printf(out, "nickname='%s'\n", info_name + 5);
}
        OSSL_STORE_INFO_free(store_info);
    }

    OSSL_STORE_close(store_ctx);

    return 1;
}
#endif /*def E_NSS_STORE_LOADER*/

#ifndef E_NSS_STORE_LOADER
typedef int e_nss_store_empty_translation_unit;
#endif
