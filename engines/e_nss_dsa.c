/**
 * NSS Engine - DSA method
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2011-2023 Roumen Petrov
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#ifndef USE_OPENSSL_PROVIDER
/* TODO: implement OpenSSL 4.0 API, as OpenSSL 3.* is quite nonfunctional */
# define OPENSSL_SUPPRESS_DEPRECATED
#endif

#ifdef E_NSS_DSA_METHOD
/* x509 is preferred to dsa, evp and etc. / 0.9.7 */
#define OPENSSL_OCSP_H /* see e_nss_store.c */
#include <openssl/x509.h>
#include <openssl/engine.h>

#include "e_nss_int.h"
#include "e_nss_err.h"

#include <nss.h>
#include <cryptohi.h>
#include <secerr.h>
/* does not exist in "old" versions */
#define SEC_ERROR_SIGNATURE_ALGORITHM_DISABLED	(SEC_ERROR_BASE + 181)


/* ===== OpenSSL DSA method compatibility code ===== */

#ifndef HAVE_DSA_METH_NEW
/* OpenSSL >= 1.1 by commits "Make DSA_METHOD opaque",
 * "Various DSA opacity fixups" and etc.
 */

/* opaque DSA method */
static inline DSA_METHOD*
DSA_meth_new(const char *name, int flags) {
    DSA_METHOD *meth;

    meth = OPENSSL_malloc(sizeof(DSA_METHOD));
    if (meth == NULL) return NULL;

    memset(meth, 0, sizeof(*meth));
    meth->name = NSS_strdup(name);
    meth->flags = flags;

    return(meth);
}


static inline void
DSA_meth_free(DSA_METHOD *meth) {
    if (meth == NULL) return;

    if (meth->name != NULL)
        OPENSSL_free((char*)meth->name);
    OPENSSL_free(meth);
}


static inline const char*
DSA_meth_get0_name(const DSA_METHOD *meth) { return meth->name; }


typedef DSA_SIG* (*DSA_METHOD_dsa_sign_f) (const unsigned char*, int, DSA*);

static inline int
DSA_meth_set_sign(DSA_METHOD *meth, DSA_METHOD_dsa_sign_f sign) {
    meth->dsa_do_sign = sign;
    return 1;
}


typedef int (*DSA_METHOD_dsa_verify_f) (const unsigned char*, int, DSA_SIG*, DSA*);

static inline DSA_METHOD_dsa_verify_f
DSA_meth_get_verify(const DSA_METHOD *meth) { return meth->dsa_do_verify; }

static inline int
DSA_meth_set_verify(DSA_METHOD *meth, DSA_METHOD_dsa_verify_f verify) {
    meth->dsa_do_verify = verify;
    return 1;
}


typedef int (*DSA_METHOD_dsa_mod_exp_f) (DSA*, BIGNUM*, BIGNUM*, BIGNUM*,
    BIGNUM*, BIGNUM*, BIGNUM*, BN_CTX*, BN_MONT_CTX*);

static inline DSA_METHOD_dsa_mod_exp_f
DSA_meth_get_mod_exp(const DSA_METHOD *meth) { return meth->dsa_mod_exp; }

static inline int
DSA_meth_set_mod_exp(DSA_METHOD *meth, DSA_METHOD_dsa_mod_exp_f dsa_mod_exp) {
    meth->dsa_mod_exp = dsa_mod_exp;
    return 1;
}


typedef int (*DSA_METHOD_dsa_bn_mod_exp_f) (DSA*, BIGNUM*, BIGNUM*,
    const BIGNUM*, const BIGNUM*, BN_CTX*, BN_MONT_CTX*);

static inline DSA_METHOD_dsa_bn_mod_exp_f
DSA_meth_get_bn_mod_exp(const DSA_METHOD *meth) { return meth->bn_mod_exp; }

static inline int
DSA_meth_set_bn_mod_exp(DSA_METHOD *meth, DSA_METHOD_dsa_bn_mod_exp_f bn_mod_exp) {
    meth->bn_mod_exp = bn_mod_exp;
    return 1;
}


typedef int (*DSA_METHOD_dsa_init_f) (DSA*);

static inline int
DSA_meth_set_init(DSA_METHOD *meth, DSA_METHOD_dsa_init_f init) {
    meth->init = init;
    return 1;
}


/* opaque DSA key */
static inline const DSA_METHOD*
DSA_get_method(const DSA *dsa) { return dsa->meth; }


static inline const ENGINE*
DSA_get0_engine(const DSA *dsa) { return dsa->engine; }


static inline void
DSA_get0_pqg(const DSA *dsa, const BIGNUM **p, const BIGNUM **q, const BIGNUM **g) {
    if (p != NULL) *p = dsa->p;
    if (q != NULL) *q = dsa->q;
    if (g != NULL) *g = dsa->g;
}

static inline int
DSA_set0_pqg(DSA *dsa, BIGNUM *p, BIGNUM *q, BIGNUM *g) {
    /* If the fields in d are NULL, the corresponding input
     * parameters MUST be non-NULL.
     *
     * It is an error to give the results from get0 on d
     * as input parameters.
     */
    if (p == dsa->p || q == dsa->q || g == dsa->g)
        return 0;

    if (p != NULL) { BN_free(dsa->p); dsa->p = p; }
    if (q != NULL) { BN_free(dsa->q); dsa->q = q; }
    if (g != NULL) { BN_free(dsa->g); dsa->g = g; }

    return 1;
}


static inline void
DSA_get0_key(const DSA *dsa, const BIGNUM **pub_key, const BIGNUM **priv_key) {
    if (pub_key  != NULL) *pub_key  = dsa->pub_key;
    if (priv_key != NULL) *priv_key = dsa->priv_key;
}

static inline int
DSA_set0_key(DSA *dsa, BIGNUM *pub_key, BIGNUM *priv_key) {
    /* If the pub_key in d is NULL, the corresponding input
     * parameters MUST be non-NULL.  The priv_key field may
     * be left NULL.
     *
     * It is an error to give the results from get0 on d
     * as input parameters.
     */
    if (pub_key == dsa->pub_key
    || (dsa->priv_key != NULL && priv_key == dsa->priv_key)
    )
        return 0;

    if (pub_key  != NULL) { BN_free(dsa->pub_key ); dsa->pub_key  = pub_key ; }
    if (priv_key != NULL) { BN_free(dsa->priv_key); dsa->priv_key = priv_key; }

    return 1;
}
#endif /*ndef HAVE_DSA_METH_NEW*/

#ifndef HAVE_DSA_SIG_SET0
/* OpenSSL >= 1.1 by commits "Implement DSA_SIG_set0() and
 * ECDSA_SIG_set0(), for setting signature values." and etc.
 */
static inline int/*bool*/
DSA_SIG_set0(DSA_SIG *sig, BIGNUM *r, BIGNUM *s) {
    if (r == NULL || s == NULL) return 0;

    BN_clear_free(sig->r);
    BN_clear_free(sig->s);

    sig->r = r;
    sig->s = s;
    return 1;
}
#endif /*ndef HAVE_DSA_SIG_SET0*/


/* ===== NSS DSA method ===== */

static int nss_dsa_ctx_index = -1;


static inline NSS_KEYCTX*
nss_get_keyctx(DSA *key) {
    return DSA_get_ex_data(key, nss_dsa_ctx_index);
}

NSS_KEYCTX*
nss_get_keyctx_dsa(EVP_PKEY *pkey) {
    NSS_KEYCTX *ret;
{/* OpenSSL >= 1.1 by commit "Add EVP_PKEY_get0_* functions."
    adds EVP_PKEY_get0_DSA with not constant return value.
    In 3.0 definition is changed to return constant!
    So throw away get0 and use existing since 0.9.5 get1 function
    with non significant performance penalty.
  */
    DSA *dsa = EVP_PKEY_get1_DSA(pkey);
    ret = nss_get_keyctx(dsa);
    DSA_free(dsa);
}
    return ret;
}


static void
NSS_KEYCTX_free_dsa(
    void *parent, void *ptr, CRYPTO_EX_DATA *ad,
    int idx, long argl, void *argp
) {
    UNUSED(parent); UNUSED(ad); UNUSED(argl); UNUSED(argp);

    CALL_TRACE("idx: %d, ptr: %p\n", idx, ptr);
    if (idx == nss_dsa_ctx_index)
         NSS_KEYCTX_free(ptr);
}


static inline NSS_CTX*
DSA_get_NSS_CTX(const DSA *key) {
    const ENGINE *e = DSA_get0_engine(/*save cast*/(DSA*)key);
    return ENGINE_get_NSS_CTX(e);
}


static DSA_SIG*
nss_dsa_do_sign(const unsigned char *dgst, int dlen, DSA *dsa) {
    DSA_SIG*    ret = NULL;
    SECStatus   rv;
    NSS_CTX    *ctx;
    NSS_KEYCTX *keyctx;

    ctx = DSA_get_NSS_CTX(dsa);
    nss_trace(ctx, "args: dgst=%p, dlen=%d\n", (void*)dgst, dlen);
{
    const DSA_METHOD *dsa_meth = DSA_get_method(dsa);
    nss_trace(ctx, "dsa=%p, meth=%p, name=%s\n", (void*)dsa, (void*)dsa_meth,
        DSA_meth_get0_name(dsa_meth));
}

    keyctx = nss_get_keyctx(dsa);
    nss_trace(ctx, "keyctx=%p\n", (void*)keyctx);
    if (keyctx == NULL) {
        NSSerr(NSS_F_DSA_DO_SIGN, NSS_R_MISSING_KEY_CONTEXT);
        goto done;
    }

    nss_trace(ctx, "keyctx->pvtkey=%p\n", (void*)keyctx->pvtkey);
    if (keyctx->pvtkey == NULL) {
        NSSerr(NSS_F_DSA_DO_SIGN, NSS_R_MISSING_PVTKEY);
        goto done;
    }

    nss_trace(ctx, "keyctx->pvtkey->keyType=%d\n", keyctx->pvtkey->keyType);

{   /* compute nss signature */
    SECItem    digest;
    SECItem    result;
    SECOidTag  hashalg = SEC_OID_UNKNOWN /*need for RSA only*/;

    digest.type = siBuffer;
    digest.data = (unsigned char*)dgst;
    digest.len  = dlen;

    result.type = siBuffer;
    result.data = NULL;
    result.len  = 0;

    rv = SGN_Digest(keyctx->pvtkey, hashalg, &result, &digest);
    nss_trace(ctx, "rv=%d\n", rv);
    if (rv != SECSuccess) {
        int port_err = PORT_GetError();
        switch(port_err) {
        case SEC_ERROR_INVALID_ALGORITHM: {
            NSSerr(NSS_F_DSA_DO_SIGN, NSS_R_INVALID_ALGORITHM);
            } break;
        case SEC_ERROR_SIGNATURE_ALGORITHM_DISABLED: {
            NSSerr(NSS_F_DSA_DO_SIGN, NSS_R_DISABLED_ALGORITHM);
            } break;
        default: {
            int port_err_off = port_err - SEC_ERROR_BASE;

            nss_trace(ctx, "port_err/ofset=%d/%d\n", port_err, port_err_off);
            NSSerr(NSS_F_DSA_DO_SIGN, NSS_R_SGN_DIGEST_FAIL);
            {/*add extra error message data*/
                char msgstr[10];
                BIO_snprintf(msgstr, sizeof(msgstr), "%d", port_err_off);
                ERR_add_error_data(2, "PORT_ERROR_OFFSET=", msgstr);
            }
            } break;
        }
        goto done_sign;
    }

    /* propagate result */
    nss_trace(ctx, "result.len=%d\n", result.len);

    ret = DSA_SIG_new();
    if (ret == NULL) {
        NSSerr(NSS_F_DSA_DO_SIGN, NSS_R_INSUFFICIENT_MEMORY);
        goto done_sign;
    }

{   /* decode signature */
    int k = result.len >> 1; /* len is even integer */
    BIGNUM *pr, *ps;

    pr = BN_bin2bn(result.data  , k, NULL);
    ps = BN_bin2bn(result.data+k, k, NULL);

    if ((pr == NULL) || (ps == NULL)
    ||  !DSA_SIG_set0(ret, pr, ps)) {
        NSSerr(NSS_F_DSA_DO_SIGN, NSS_R_INSUFFICIENT_MEMORY);
        BN_free(pr);
        BN_free(ps);
        DSA_SIG_free(ret);
        ret = NULL;
    }
}

done_sign:
    if (result.data != NULL) {
        PORT_Free(result.data);
    }
}   /* end of nss signature computation */

done:
    return ret;
}


static int
nss_dsa_init(DSA *key) {
    int ret = 0;

    CALL_TRACE("key=%p\n", (void*)key);

{   /* setup NSS DSA key context */
    NSS_KEYCTX *keyctx = NSS_KEYCTX_new();
    CALL_TRACE("idx=%d, keyctx=%p\n", nss_dsa_ctx_index, (void*)keyctx);
    if (keyctx == NULL) {
        NSSerr(NSS_F_DSA_INIT, NSS_R_INSUFFICIENT_MEMORY);
        goto done;
    }
    DSA_set_ex_data(key, nss_dsa_ctx_index, keyctx);
}

    ret = 1;
done:
    CALL_TRACE("^ %s\n", (ret ? "ok": ""));
    return ret;
}


NSS_KEYCTX*
nss_init_keyctx_dsa(EVP_PKEY *pkey, ENGINE *e) {
    NSS_KEYCTX *ret = NULL;
    DSA *dsa;
    DSA *pkey_dsa;

    pkey_dsa = EVP_PKEY_get1_DSA(pkey);
    CALL_TRACE("pkey=%p, key=%p\n", (void*)pkey, (void*)pkey_dsa);
#ifdef HAVE_EVP_KEYMGMT_GET0_PROVIDER	/* OpenSSL >= 3.0 */
    /* FIPS enabled OpenSSL 3+ fail to return "elementary" key */
    if (pkey_dsa == NULL) {
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INVALID_ARGUMENT);
        return NULL;
    }
#endif

/* NOTE: OpenSSL 3+ wrongly assumes that key associated by
 * default with "external" method is a OpenSSL managed key!
 * So log only and recreate key.
 */
    if (ENGINE_get_DSA(e) == DSA_get_method(pkey_dsa)) {
        CALL_TRACE("DSA by default!\n");
#ifndef HAVE_EVP_KEYMGMT_GET0_PROVIDER	/* OpenSSL 3+ */
        ret = nss_get_keyctx(pkey_dsa);
        DSA_free(pkey_dsa);
        return ret;
#endif
    }
    /* else dsa method is not associated with this engine ... */
    CALL_TRACE("new DSA ...\n");

    dsa = DSA_new_method(e);
    if (dsa == NULL) {
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INSUFFICIENT_MEMORY);
        goto err;
    }

{ /* copy algorithm parameters */
    BIGNUM *p = NULL, *q = NULL, *g = NULL;

    DSA_get0_pqg(pkey_dsa, (const BIGNUM**)&p, (const BIGNUM**)&q, (const BIGNUM**)&g);
    p = BN_dup(p);
    q = BN_dup(q);
    g = BN_dup(g);
    if ((p == NULL) || (q == NULL) || (g == NULL)
    ||  !DSA_set0_pqg(dsa, p, q, g)
    ) {
        BN_free(p);
        BN_free(q);
        BN_free(g);
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INSUFFICIENT_MEMORY);
        goto err;
    }
}
{ /* copy public key */
    BIGNUM *pub_key = NULL;

    DSA_get0_key(pkey_dsa, (const BIGNUM**)&pub_key, NULL);
    pub_key = BN_dup(pub_key);
    if (pub_key == NULL
    ||  !DSA_set0_key(dsa, pub_key, NULL)
    ) {
        BN_free(pub_key);
        NSSerr(NSS_F_INIT_KEYCTX, NSS_R_INSUFFICIENT_MEMORY);
        goto err;
    }
}

    if (EVP_PKEY_set1_DSA(pkey, dsa))
        ret = nss_get_keyctx(dsa);

err:
    DSA_free(pkey_dsa);
    DSA_free(dsa);
    return ret;
}


int/*bool*/
bind_nss_dsa_method(ENGINE *e) {
    DSA_METHOD *nss_dsa_method = DSA_meth_new("NSS DSA method",
    #ifdef DSA_FLAG_FIPS_METHOD
        DSA_FLAG_FIPS_METHOD |
    #endif
        0  /* int flags; */
    );
    if (nss_dsa_method == NULL) return 0;

    /* ensure DSA context index */
    if (nss_dsa_ctx_index < 0)
        nss_dsa_ctx_index = DSA_get_ex_new_index(0,
            NULL, NULL, NULL, NSS_KEYCTX_free_dsa);

    CALL_TRACE("nss_dsa_ctx_index=%d\n", nss_dsa_ctx_index);
    if (nss_dsa_ctx_index < 0) goto err;

    if (!DSA_meth_set_sign(nss_dsa_method, nss_dsa_do_sign)
    ||  !DSA_meth_set_init(nss_dsa_method, nss_dsa_init)
    )
        goto err;

{   /* reuse some functions from OpenSSL DSA method */
    const DSA_METHOD *dsa_method = DSA_OpenSSL();

    CALL_TRACE("dsa_method=%p[%s]\n", (void*)dsa_method,
        (dsa_method ? DSA_meth_get0_name(dsa_method) : ":"));
    if (dsa_method == NULL)
        return 0;

    if (!DSA_meth_set_verify(nss_dsa_method, DSA_meth_get_verify(dsa_method))
    ||  !DSA_meth_set_mod_exp(nss_dsa_method, DSA_meth_get_mod_exp(dsa_method))
    ||  !DSA_meth_set_bn_mod_exp(nss_dsa_method, DSA_meth_get_bn_mod_exp(dsa_method))
    )
        goto err;
}

    if (ENGINE_set_DSA(e, nss_dsa_method))
        return 1;

err:
    DSA_meth_free(nss_dsa_method);
    return 0;
}


void
destroy_nss_dsa_method(ENGINE *e) {
    DSA_METHOD *dsa_method;

    dsa_method = (DSA_METHOD*)ENGINE_get_DSA(e);
    (void)ENGINE_set_DSA(e, NULL);
    DSA_meth_free(dsa_method);
}
#endif /*def E_NSS_DSA_METHOD*/

#ifndef E_NSS_DSA_METHOD
typedef int e_nss_dsa_empty_translation_unit;
#endif
