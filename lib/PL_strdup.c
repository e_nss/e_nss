/**
 * NSS Engine - compat library
 *
 * PL_strdup as work arround for broken nss libraries
 * (build of library with linker flag --as_neeed )
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2017 Roumen Petrov
 */

#include <prmem.h>
#include <string.h>

char*
PL_strdup(const char *s) {
    size_t l;

    if (s == NULL) return NULL;

    l = strlen(s) + 1;

{   char *r = PR_Malloc(l);

    if (r == NULL) return NULL;

    return memmove(r, s, l);
}
}
