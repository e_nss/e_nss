/**
 * NSS Engine - compat library
 *
 * dummy compilation unit as building an empty library is not portable
 *
 * This is free software; see Copyright file in the source
 * distribution for precise wording.
 *
 * Copyright (C) 2017 Roumen Petrov
 */


static void e_nss_extra(void) {
    /* static library cannot be created empty */
}

void (*f_e_nss_extra)(void) = e_nss_extra;
